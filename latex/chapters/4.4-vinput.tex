\section{Gestion des périphériques d'entrée}

\subsection{Événements d'entrée}

Le module \textit{input} est un module du noyau Linux par lequel passe tous les événements d'entrée, qu'ils proviennent d'une souris, d'un clavier, d'un écran tactile ou encore d'autres périphériques d'entrée. L'existence de ce module a grandement simplifié l'écriture du backend et frontend des périphériques d'entrée, nommé vinput.

En effet, Linux possédant déjà de multiples drivers pour les périphériques d'entrée, il n'est pas nécessaire de se préoccuper des particularités de chacun, comme cela a été fait avec le PL050. De plus, pour chaque événement reçu, le driver du périphérique appelle la fonction \texttt{input\_event} définie dans le module input de Linux (\texttt{input.c}). Un tel événement \cite{linux-input} est composé de trois valeurs (déclarées dans \texttt{include/uapi/linux/input-event-codes.h}) :

\begin{itemize}
    \item \texttt{unsigned int type} : désigne le type d'événement, il en existe une douzaine mais ceux qui sont intéressants pour ce travail sont \texttt{EV\_KEY} (e.g. touches de clavier, boutons de souris), \texttt{EV\_REL} (déplacement de la souris) et \texttt{EV\_ABS} (déplacement de l'écran tactile).
    \item \texttt{unsigned int code} : un code identifiant plus précisément l'événement, par exemple le code 42 pour le type \texttt{EV\_KEY} désigne la touche du clavier \textit{Left Shift}, le code 0 pour le type \texttt{EV\_REL} désigne le déplacement de la souris selon l'axe des abscisses.
    \item \texttt{int value} : la valeur de l'événement, pour une touche clavier, indique si elle a été appuyée ou relâchée, pour un déplacement de la souris c'est la valeur de ce déplacement qui est indiqué.
\end{itemize}

Afin de récupérer ces événements dans le backend vinput, il suffit de modifier la fonction \texttt{input\_event} afin d'appeler la fonction \texttt{vinput\_pass\_event} définie dans le backend qui s'occupera de transmettre l'événement au frontend.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.69\columnwidth]{images/vinput-gen.pdf}
    \caption{Transmission des événements d'entrée des drivers Linux vers le backend vinput}
\end{figure}

\subsection{Création du backend et frontend}

Le processus de création du backend et du frontend est très similaire à celui du framebuffer. Il est basé sur vdummy et a été placé dans le répertoire \texttt{vinput}. Il faut créer un nouveau nœud dans la DTS Linux et celle de la ME, ainsi que modifier les fichiers Kconfig correspondants afin que les drivers puissent être activés ou désactivés dans la configuration du noyau.

La différence principale entre le vfb et vinput se trouve dans la communication entre le backend et le frontend. Dans vfb, trois propriétés dans VBStore ont suffit. Par contre, utiliser VBStore pour l'envoi d'événements n'est pas adapté car il est possible qu'ils soient produits (par l'agence) plus vite qu'ils ne soient consommés (par la ME).

Il est plus adapté d'utiliser un anneau partagé entre le backend et le frontend. Cet anneau permet au backend de déposer des réponses sur l'anneau, le frontend peut ensuite lire les réponses à sa guise. Le backend notifie le frontend via une interruption, ce qui est très similaire au fonctionnement du PL050 et des périphériques d'entrée en général. De la même manière, le frontend peut déposer des requêtes sur l'anneau et notifier le backend. À noter qu'une réponse ne présuppose pas une requête, les deux notions sont indépendantes.

Dans le cas du vinput, seules les réponses sont nécessaires car les événements d'entrées ne sont transmis que du backend vers le frontend. Une réponse correspond à une structure. Afin de transmettre les événements d'entrée, la structure \texttt{vinput\_response\_t} a été déclarée telle que :

\begin{lstlisting}[language=C,caption=Backend vinput : structure de la réponse]
typedef struct  {
	unsigned int type;
	unsigned int code;
	int value;
} vinput_response_t;
\end{lstlisting}

\subsection{Gestion des ME}

Lorsqu'un frontend est connecté, la fonction \texttt{vinput\_connected} du backend est exécutée. Cette fonction a été modifiée pour garder une référence des frontend connectés. Un tableau a été créé à cet effet et contient un pointeur sur une structure \texttt{vinput\_t}. L'indice du tableau correspond à l'identifiant du domaine.

Lorsque le domaine actif change suite à un double Ctrl + A, \texttt{vinput\_set\_current} est appelée. Cette fonction enregistre simplement l'identifiant du domaine actif passé en paramètre dans la variable \texttt{current\_vinput}.

Ainsi, afin de savoir à quel frontend transmettre les événements d'entrée, il suffit d'accéder à l'élément \texttt{current\_vinput} du tableau \texttt{vinputs}. Il est important de restreindre les événements d'entrée à la seule ME active, car ces événements proviennent de l'utilisateur qui voit à l'écran l'interface graphique de la ME active.

\subsection{Transmission des événements}

La figure \ref{fig:vinput-ring} montre l'utilisation de l'anneau partagé afin de transmettre les événements d'entrée du backend au frontend et ensuite vers les drivers virtuels.

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/vinput-ring.pdf}
    \caption{Transmission des événements d'entrée du backend au frontend via l'anneau partagé}
    \label{fig:vinput-ring}
\end{figure}

\subsubsection{Du backend au frontend}

La fonction \texttt{vinput\_pass\_event}, appelée par le module input de Linux, fait usage de l'anneau partagé afin de transmettre les événements au frontend.

\begin{lstlisting}[language=C,caption=Backend vinput : envoi d'une réponse au frontend]
void vinput_pass_event(unsigned int type, unsigned int code, int value)
{
	vinput_response_t *ring_rsp;
	vinput_t *vinput = vinputs[current_vinput];

	/* If the frontend is not connected, skip. */
	if (!vinput)
		return;

	/* Send event to front-end. */
	ring_rsp = vinput_ring_response(&vinput->ring);
	ring_rsp->type = type;
	ring_rsp->code = code;
	ring_rsp->value = value;

	vinput_ring_response_ready(&vinput->ring);
	notify_remote_via_virq(vinput->irq);
}
\end{lstlisting}

La fonction \texttt{notify\_remote\_via\_irq} permet, comme son nom l'indique, de notifier le frontend de l'existence d'une nouvelle réponse via une interruption. Cela aura pour effet d'exécuter la fonction \texttt{vinput\_interrupt} du frontend, puisqu'elle a été définie comme étant le gestionnaire d'interruptions. L'extrait de code suivant montre comment récupérer la réponse du backend.

\begin{lstlisting}[language=C,caption=Backend vinput : envoi d'une réponse au frontend]
irq_return_t vinput_interrupt(int irq, void *dev_id)
{
	struct vbus_device *vdev = (struct vbus_device *) dev_id;
	vinput_t *vinput = to_vinput(vdev);
	vinput_response_t *ring_rsp;
	unsigned int type, code;
	int value;

	while ((ring_rsp = vinput_ring_response(&vinput->ring)) != NULL) {

		type = ring_rsp->type;
		code = ring_rsp->code;
		value = ring_rsp->value;

		/* Handle input event. */
	}

	return IRQ_COMPLETED;
}
\end{lstlisting}

\subsubsection{Du frontend aux drivers virtuels}

Deux drivers virtuels ont été créés dans la ME : \texttt{so3virt\_kbd.c} pour la clavier et \texttt{so3virt\_mse.c} pour la souris. Leur but est la création d'un fichier \texttt{/dev/keyboard0} et \texttt{/dev/mouse0} depuis l'espace utilisateur mais aussi de recevoir et traiter les événements reçus du frontend. Puisque ces drivers n'interagissent pas avec un contrôleur de périphérique comme le PL050, le protocole PS2 n'est plus utilisé, simplifiant grandement ces drivers.

Afin qu'aucune modification ne soit nécessaire dans l'espace utilisateur, les drivers doivent réagir aux mêmes opérations \texttt{ioctl} définies dans le driver du PL050. C'est-à-dire retourner la dernière touche appuyée pour le driver du clavier et retourner les coordonnées de la souris pour le driver de la souris.

\paragraph*{Driver clavier}

Dans le driver du clavier, la fonction \texttt{so3virt\_kbd\_event} a été créée et permet au frontend de passer les événements clavier au driver virtuel. Dans cette fonction, le driver doit transformer les événements d'entrée en un caractère UTF-8. Il faut détecter si la touche Shift est appuyé lorsqu'un événement correspondant à une lettre est reçu. Si c'est le cas alors il faut retourner la lettre majuscule correspondante.

À noter que si une touche est relâchée, sa valeur n'est pas mise à zéro dans la fonction \texttt{so3virt\_kbd\_event}, mais seulement une fois qu'elle a été lue par un appel \texttt{ioctl}. Cela afin d'être sûr que le client ait le temps de lire la touche. Un système plus robuste aurait pu être implémenté en utilisant une file où les événements seraient lus par le driver virtuel lors d'un appel \texttt{ioctl}.

S'il avait été nécessaire de gérer des touches (ou combinaison de touches) qui ne retournent pas un caractère mais qui exécutent une action (e.g. raccourci clavier), il aurait été plus judicieux de gérer les événements dans l'espace utilisateur, ces actions étant propres à chaque application.

\paragraph*{Driver souris}

Dans le driver de la souris, la fonction \texttt{so3virt\_mse\_event} a été créée et permet au frontend de passer les événements relatif à la souris au driver virtuel. Dans cette fonction, les coordonnées absolues de la souris doivent être calculées et les boutons de la souris doivent être enregistrés.

Les événements d'entrée de type \texttt{EV\_REL} n'envoie pas les coordonnées absolues de la souris mais le déplacement de celle-ci par rapport à l'événement précédent et selon un axe (abscisse ou ordonnée). Il faut alors calculer les coordonnées absolues de la souris tout comme cela est fait pour le driver PL050.

\paragraph*{Séparation des événements clavier–souris}

Dans la fonction \texttt{vinput\_interrupt} le frontend doit déterminer quels événements sont envoyés au clavier et lesquels à la souris. Cela a été fait en regardant le type de l'événement mais aussi le code.

\begin{lstlisting}[language=C,caption=Frontend vinput : tri des événements clavier–souris]
while ((ring_rsp = vinput_ring_response(&vinput->ring)) != NULL) {

	type = ring_rsp->type;
	code = ring_rsp->code;
	value = ring_rsp->value;

	if (type == EV_REL || /* is mouse movement */
	   (type == EV_KEY && /* is mouse button click */
         (code == BTN_LEFT || code == BTN_MIDDLE || code == BTN_RIGHT)) {
		so3virt_mse_event(type, code, value);
	}
	else if (type == EV_KEY) {
		so3virt_kbd_event(type, code, value);
	}

	/* Skip unhandled events. */
}
\end{lstlisting}