\chapter{SO3}
\label{ch:so3}

SO3 est un système d'exploitation entièrement écrit par le REDS à partir de zéro et dédié exclusivement aux processeurs ARM. Il n'est pas basé sur le noyau Linux et est un OS plus léger mais aussi plus facile à étudier et à modifier par des personnes débutant en développement de systèmes d'exploitation.

Le chapitre précédent a décrit le fonctionnement de LittlevGL mais a aussi introduit les modifications qui doivent être apportées à SO3 afin que cette librairie puisse fonctionner. Ces modifications seront détaillées dans ce chapitre.

\section{Environnement de développement et d'exécution}

\subsection{QEMU}

Afin de faciliter le développement de SO3, l'OS n'est pas déployé à chaque modification sur un système embarqué telle que la Raspberry Pi 4 mais est exécuté sur une plateforme émulée par le logiciel QEMU. Cela permet de tester les modifications apportées plus rapidement.

Actuellement, le REDS utilise QEMU pour émuler la plateforme ARM Versatile Express avec le processeur Cortex-A15. Une fois SO3 lancé sur cette plateforme, il est possible de communiquer avec via une interface UART. Cette interface permet d'envoyer et de recevoir des caractères entre l'OS utilisé pour le développement et SO3. Cependant, il est important de comprendre que cela ne nécessite du côté de SO3 ni un driver pour le clavier ni pour le framebuffer. Il faut un driver UART mais celui-ci existe déjà dans SO3.

SO3 n'ayant pas de support graphique, QEMU est lancé avec l'option \texttt{-nographic} qui désactive la sortie graphique. Il est nécessaire de retirer cette option afin que QEMU puisse afficher par la suite le contenu du framebuffer. L’option \texttt{-display gtk} peut aussi être utilisée mais \texttt{gtk} en est la valeur par défaut. Aussi, QEMU doit avoir été compilé avec l’option \texttt{-–enable-gtk} pour que l’option précédente fonctionne. Une fois ces manipulations faites et QEMU lancé, une nouvelle fenêtre affichera le message \textit{Guest has not initialized the display (yet)}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\columnwidth]{images/qemu-no.png}
    \caption{Fenêtre QEMU pour l'affiche de l'interface graphique}
\end{figure}

\subsection{ARM Versatile Express}

La plateforme Versatile Express est composée de plusieurs contrôleurs de périphérique chacun ayant une fonction définie. Pour l'affichage de l'interface graphique, le contrôleur PL111 \cite{arm-pl111} (\textit{PrimeCell ColorLCD Controller}) permet de gérer le framebuffer. Pour communiquer avec la souris et le clavier, il faut passer par le contrôleur PL050 \cite{arm-pl050} (\textit{PrimeCell PS2 Keyboard/Mouse Interface}). Il s'agit donc d'écrire un driver pour chacun de ces contrôleurs afin que SO3 puisse les utiliser.

\section{Fichiers spéciaux}
\label{sec:specfile}

Comme cela a été vu lors de l'utilisation de LittlevGL, afin de pouvoir communiquer avec le driver d'un contrôleur depuis l'espace utilisateur, il est nécessaire que le driver soit représenté par un fichier spécial. Ce fichier est dit spécial car il n'a pas de contenu et n'existe pas dans le système de fichiers. Cependant, il est considéré comme un fichier car il est ouvert avec l'appel système \texttt{open} qui retourne un descripteur de fichier.

Pour le nom de ces fichiers, il a été décidé d'utiliser la convention suivante et proche de ce qui est fait sur les systèmes inspirés d'Unix : \texttt{/dev/<device-class><device-id>}. \texttt{device-class} représente le type de contrôleur (e.g. \texttt{fb} pour les framebuffers, \texttt{mouse} pour les souris) et \texttt{device-id} permet de numéroter les contrôleurs de même type. Ces valeurs sont définies dans \texttt{so3/include/device/device.h}.

\subsection{Virtual Filesystem}

Le point d'entrée depuis l'espace utilisateur est l'appel système \texttt{open} qui est implémenté dans la fonction \texttt{do\_open} de \texttt{so3/fs/vfs.c}, qui est la couche VFS (\textit{Virtual Filesystem}) de SO3. Le VFS gère la manipulation de fichiers et de répertoires indépendemment du système de fichiers sous-jacent. Actuellement, SO3 utilise FAT. Si cela devait changer, il n'y aurait besoin d'adapter seulement le lien entre le VFS et le nouveau système de fichiers.

Actuellement, l'appel système \texttt{open} considère tout fichier passé en paramètre comme existant sur le système de fichiers. Cette implémentation doit donc être modifiée afin que les fichiers commençant par \texttt{/dev/} soient considérés comme des fichiers spéciaux.

Lors de l'ouverture d'un fichier par \texttt{do\_open}, une structure de type \texttt{fd} est créée. Celle-ci est ensuite associée au descripteur de fichier global grâce au tableau \texttt{open\_fds}. Un descripteur de fichier local est aussi créé et fait référence à un descripteur global mais est propre à chaque processus. C'est ce descripteur local qui est retourné dans l'espace utilisateur.

Cette structure est importante car elle contient une structure de type \texttt{file\_operations} qui contient les opérations sur fichier définies pour le fichier en question. Pour les fichiers présents sur le système de fichiers, c'est ce dernier qui définit les opérations possibles. Par contre, pour les fichiers spéciaux, ces opérations sont propres à chaque périphérique, elles doivent donc être définies dans le driver du périphérique.

Il existe une correspondance entre ces opérations et les appels système. Par exemple, l'appel système \texttt{open} exécutera l'opération \texttt{open} du fichier. De même pour \texttt{read}, \texttt{write}, \texttt{close} ainsi que \texttt{ioctl}, \texttt{mmap} et d'autres.

La figure \ref{fig:vfs-open} montre comment est utilisé le VFS lors de l'ouverture d'une fichier. Le VFS récupère les opérations sur fichier d'une source parmi plusieurs (\textit{NTFS} et \textit{Drivers} n'existe pas dans l'implémentation actuelle, ils sont mentionnés à titre d'exemple) puis les stocke dans la structure \texttt{fd} du fichier ouvert. Ces opérations peuvent alors être récupérées ultérieurement.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{images/vfs-open.pdf}
    \caption{Interactions avec le VFS lors de l'ouverture d'un fichier}
    \label{fig:vfs-open}
\end{figure}

\subsection{Solution implémentée}

Il faut donc, depuis la fonction \texttt{do\_open}, être capable de récupérer les opérations sur fichier du driver correspondant au nom de fichier passé en paramètre. Pour cela, la structure \texttt{devclass} a été créée. Elle contient, entre autres, le type du contrôleur (e.g. \texttt{fb}) et les opérations sur fichier qui seront définies par le driver. Dans le fichier \texttt{so3/device/device.c}, une liste de structures \texttt{devclass} a été créée.

Pour rajouter un élément à cette liste, le driver utilise la fonction \texttt{devclass\_register}. De son côté, le VFS utilise la fonction \texttt{devclass\_get\_fops} pour récupérer les opérations sur fichier du driver. Ces deux fonctions sont aussi définies dans \texttt{device.c}.

Le code source suivant illustre comment un driver déclare ses opérations. À noter qu'un driver n'est pas obligé de le faire, tous les drivers n'ont pas besoin d'être accessibles depuis l'espace utilisateur.

\begin{lstlisting}[language=C,caption=Enregistrement des opérations sur fichier par un driver]
struct file_operations driver_fops = {
    /* file operations to be defined */
};

struct devclass driver_cdev = {
    .class = DEV_CLASS_, /* device class, defined in device.h */
    .fops = &driver_fops,
};

int driver_init(dev_t *dev)
{
    devclass_register(dev, &driver_cdev);
}
\end{lstlisting}

L'extrait suivant montre une version simplifiée de \texttt{do\_open} et de comment le VFS récupère les opérations sur fichier définie ci-dessus par le driver.

\begin{lstlisting}[language=C,caption=Récupération des opérations sur fichier dans l'appel système \texttt{open}]
int do_open(const char *filename, int flags)
{
    int fd, gfd;
    struct file_operations *fops;
    
	/* Special file. */
	if (!strncmp(DEV_PREFIX, filename, DEV_PREFIX_LEN)) {

        /* Get fops defined by the driver. */
		fops = devclass_get_fops(filename + DEV_PREFIX_LEN, &type);
	}
	/* File on the filesystem. */
	else {
	    /* Get fops of the FAT filesystem. */
		fops = registered_fs_ops[0];
	}
	
	/* Create struct fd entry in open_fds and stores fops. */
	fd = vfs_open(filename, fops, type);
    
    /* Global file descriptor, created in vfs_open. */
    gfd = current()->pcb->fd_array[fd];
	
	/* Call the open file operation, if defined. */
	if (fops->open)
	    fops->open(gfd, filename);

    /* Return local file descriptor. */
	return fd;
}
\end{lstlisting}

Ainsi, avec ce nouveau système mis en place, il devient possible de récupérer depuis l'espace utilisateur un descripteur de fichier lié à un périphérique. Avec ce descripteur, il est ensuite possible d'exécuter d'autres opérations comme \texttt{mmap} et \texttt{ioctl}, ce qui nous sera utile par la suite.

La figure \ref{fig:specfileso3} illustre l'implémentation des fichiers spéciaux dans SO3, en comparant notamment quand est-ce que le driver enregistre ses opérations sur fichier et quand est-ce qu'elles sont récupérées par le VFS.

\begin{figure}[ht]
    \centering
    \includesvg[width=\columnwidth]{images/recap.svg}
    \caption{Vue général de l'implémentation des fichiers spéciaux dans SO3}
    \label{fig:specfileso3}
\end{figure}

\section{Driver du framebuffer}

Afin de pouvoir écrire le driver du framebuffer, il est d'abord nécessaire d'identifier le contrôleur correspondant de la plateforme utilisée. Sur la plateforme ARM Versatile Express, ce contrôleur est le PL111 \cite{arm-pl111-main}.

Le répertoire \texttt{fb} a été créé dans \texttt{so3/devices} et le driver y a été écrit dans le fichier \texttt{pl111.c}.

\subsection{Description du PL111}

Le PL111 possède un certain nombre de registres qui permettent de l'activer et de le configurer. La figure \ref{fig:pl111regs} extraite du document \textit{PL111 Technical Reference Manual} \cite{arm-pl111-main} détaille une partie de ces registres.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\columnwidth]{images/pl111-regs.png}
    \caption{Registres du PL111 tels que décrits dans la documentation ARM}
    \label{fig:pl111regs}
\end{figure}

\subsubsection{Activation}
\label{subsubsec:init}

La procédure de mise en route du contrôleur est décrite dans la section 1.1.8 de ce document. Il est indiqué qu'il faut activer (i.e. mettre à 1) deux bits (LcdEn et LcdPwr) du registre de contrôle (détaillé dans la figure \ref{fig:lcdcontrol}). La modification de ces deux bits fera disparaître le message \textit{Guest has not initialized the display (yet)} affiché par QEMU.

\subsubsection{Position du framebuffer}

Le PL111 peut gérer un ou deux panels en fonction de sa configuration (bit LcdDual du registre LCDControl). Dans notre cas, seul un panel est nécessaire, le bit LcdDual est donc mis à 0.

Le PL111 nous permet de définir les emplacements des deux framebuffers dans la mémoire vive. L'adresse du premier panel doit être mise dans le registre LCDUPBASE (\textit{\textbf{u}pper \textbf{p}anel}), celle du deuxième panel dans LCDLPBASE (\textit{lower panel}).

Ce sont des adresses physiques qui doivent être fixées et elles doivent être alignées sur quatre octets (i.e. les deux derniers bits sont à 0). Puisque nous n'utilisons qu'un seul panel, LCDLPBASE est mis à 0. La plateforme Versatile Express dédie 32 Mo de mémoire vidéo (\textit{Video SRAM} ou simple VRAM) située à l'adresse \texttt{0x18000000}. Cette adresse a été utilisée pour LCDUPBASE.

La figure \ref{fig:vram} montre différents schémas permettant de trouver cette adresse. Les schémas sont extraits du document \textit{Motherboard Express $\mu$ATX Technical Reference Manual}.

\begin{figure}[hp]
    \centering
    \includegraphics[width=\columnwidth]{images/vram.png}
    \caption{L'adresse physique de la VRAM est \texttt{0x18000000}}
    \label{fig:vram}
\end{figure}

\subsubsection{Définition de la résolution}

La résolution d'un écran LCD est définie par ses paramètres de \textit{timing}. Il y a quatre registres de timing numérotés de 0 à 3. Les deux premiers sont les plus importants et permettent de configurer la résolution de l'écran.

Dans le premier de ces registres, six bits code le nombre de pixels par ligne (\textit{pixels-per-line}, PPL). Cette valeur est définie par la formule $H / 16 - 1$, H étant la résolution horizontale voulue. Dans le deuxième registre, dix bits code le nombre de lignes par panel (\textit{lines-per-panel}, LPP). Elle est définie comme étant la résolution verticale moins 1.

Ces deux registres possèdent d'autres valeurs qui doivent être renseignées. Des exemples de valeurs pour les différentes propriétés de ces deux premiers registres peuvent être trouvées dans divers fichiers DTS (\textit{Device Tree Source}) du noyau Linux.

Les deux derniers registres permettent de configurer différentes propriétés relatives aux signaux d'horloges.

\subsubsection{Registre de contrôle}

Le registre de contrôle est un des plus importants car il permet d'initialiser le contrôleur comme expliquer dans la section \ref{subsubsec:init}. Il permet aussi de définir, entre autres, le nombre de bits par pixel (\textit{bits per pixel}, bpp), le type d'écran (TFT ou STN), le nombre de panels (1 ou 2) et si l'affichage du rouge et du bleu doit être inversé ou pas (BGR ou RGB).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\columnwidth]{images/controlreg.png}
    \caption{Détail des bits du registre LCDControl \cite{arm-pl111-main}}
    \label{fig:lcdcontrol}
\end{figure}

\subsubsection{Écriture des registres}

L'écriture des valeurs susmentionnées dans les registres correspondants se fait dans la fonction d'initialisation \texttt{pl111\_init} du driver. Celle-ci est enregistrée auprès du noyau grâce à la macro \texttt{REGISTER\_DRIVER\_POSTCORE}. Dans SO3, il existe des drivers CORE et POSTCORE, la distinction est faite pour permettre au noyau d'initialiser les drivers CORE en premier. La modification des registres se fait grâce à la fonction \texttt{iowrite32}.

\subsection{Mapping de la mémoire}

Dans l'application utilisateur utilisant LittlevGL, après avoir obtenu un descripteur de fichier sur le framebuffer, l'appel système \texttt{mmap} est utilisé afin de mapper une partie de la mémoire du processus vers celle du framebuffer. L'emplacement de la mémoire du processus est communiqué par l'appel \texttt{mmap} de la libc, donc depuis l'espace utilisateur.

L'implémentation de \texttt{mmap} dans le noyau a été rajoutée dans le VFS (fonction \texttt{do\_mmap}). Son implémentation est simple. D'abord, le nombre de page devant être mappées est calculé en divisant la taille de la zone mémoire a être mappée (passée en paramètre) par la taille d'une page (constante du noyau). Ensuite, en utilisant le descripteur de fichier local passé en paramètre, le descripteur global est récupéré et à partir de celui-ci, les opérations sur fichier que le driver a définit. \texttt{mmap} étant une de ces opérations, elle est exécutée.

Ainsi, la logique centrale de l'appel système \texttt{mmap} est définie par le driver, et peut donc être différente pour chaque périphérique. Cette séparation est très similaire à ce qui est fait par les OS inspirés d'Unix.

Dans le driver du PL111, l'opération \texttt{mmap} est rajoutée. L'opération dispose de l'adresse virtuelle du processus à partir de laquelle doit être fait le mapping. Cette adresse est décidée par l'espace utilisateur. L'opération dispose aussi du nombre de pages à être mappées. Un mapping est effectué entre l'adresse virtuelle et l'adresse physique de la VRAM grâce à la fonction \texttt{create\_mapping} définie dans \texttt{mmu.c}.

\begin{lstlisting}[language=C,caption=Implémentation de \texttt{mmap} par le PL111]
void *fb_mmap(int fd, uint32_t virt_addr, uint32_t page_count)
{
	uint32_t i;

	for (i = 0; i < page_count; i++)
		create_mapping(current()->pcb->pgtable,
		               virt_addr + i * PAGE_SIZE,
		               LCDUPBASE + i * PAGE_SIZE,
		               PAGE_SIZE, false);

	return (void *) virt_addr;
}

struct file_operations pl111_fops = {
	.mmap = fb_mmap
};
\end{lstlisting}

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/mmap-pl111.pdf}
    \caption{Vue générale du mapping de la mémoire du framebuffer}
\end{figure}

\subsection{Propriétés du framebuffer}

Afin que l'espace utilisateur puisse correctement appeler l'appel système \texttt{mmap}, la taille de la zone mémoire correspondant au framebuffer doit être connue. Cette taille dépend de la résolution de l'écran (définie par le driver comme vu précédemment) et du nombre de bits par pixel (aussi défini par le driver).

La résolution de l'écran doit aussi être connue par l'application utilisateur pour être indiquée au driver d'affichage de LittlevGL ainsi qu'au driver de la souris afin que les coordonnées de celle-ci soient limitées par la taille de l'écran.

Afin de pouvoir récupérer ces propriétés, l'appel système \texttt{ioctl} a été utilisé. Cet appel existe déjà et n'a pas requis de modifications. Il a fallu cependant implémenter les opérations voulues dans le driver du PL111, à la manière de ce qui a été fait pour \texttt{mmap}.

\texttt{ioctl} prend en argument l'identifiant de la commande devant être exécutée ainsi qu'un entier qui est utilisé pour récupérer d'autres paramètres ou renvoyer une valeur de retour (il faut alors l'utiliser comme un pointeur). L'identifiant est définit par le driver mais il doit être constant, afin qu'il puisse aussi être connu de l'espace utilisateur.

Le code suivante montre l'implémentation de \texttt{ioctl} dans le driver.

\begin{lstlisting}[language=C,caption=Implémentation de l'opération \texttt{ioctl} par le PL111]
int fb_ioctl(int fd, unsigned long cmd, unsigned long args)
{
	switch (cmd) {

	case IOCTL_HRES: /* Horizontal resolution. */
		*((uint32_t *) args) = HRES;
		return 0;

	case IOCTL_VRES: /* Vertical resolution. */
		*((uint32_t *) args) = VRES;
		return 0;

	case IOCTL_SIZE: /* Framebuffer size. */
		*((uint32_t *) args) = HRES * VRES * 4; /* assume 24bpp */
		return 0;

	default: /* Unknown command. */
		return -1;
	}
}
\end{lstlisting}

\section{Drivers du clavier et de la souris}

À nouveau, l'identification du contrôleur permettant de communiquer avec le clavier et la souris est essentiel. Sur la plateforme ARM Versatile Express, ce contrôleur est le PL050 \cite{arm-pl050-main} et il y est présent en deux exemplaires. Le premier permet de communiquer avec le clavier (nommé KMI0) et le deuxième avec la souris (KMI1).

PS2 est le protocole utilisé pour la communication entre le driver et les périphériques d'entrée que sont le clavier et la souris.

Le répertoire \texttt{input} a été créé dans \texttt{so3/devices}. Un driver pour le clavier et un autre pour la souris ont été écrits et sont répartis dans plusieurs fichiers :

\begin{itemize}
    \item \texttt{pl050.c} : code commun utilisé autant par la souris que par le clavier.
    \item \texttt{kmi0.c} : driver pour le clavier.
    \item \texttt{kmi1.c} : driver pour la souris.
    \item \texttt{ps2.c} : code utilitaire pour faciliter la communication avec la souris et le clavier.
\end{itemize}

\subsection{Description du PL050}

\subsubsection{Activation}

L'initialisation du contrôleur passe par deux registres : KMICLKDIV (\textit{clock divisor register}) et KMICR (\textit{control register}).

Indiqué dans la documentation ARM \cite{arm-pl050-main}, l'horloge de référence doit générer un signal de 8 MHz. Il faut alors spécifier une valeur dans le registre KMICLKDIV qui sera utilisée pour diviser l'horloge de référence afin d'obtenir ces 8 MHz. La valeur 2 a été reprise du driver Linux.

Il faut ensuite modifier le registre de contrôle (KMICR) afin d'activer le contrôleur (bit KmiEn) et les interruptions émises lorsque des données sont reçues du périphérique (bit KMIRXINTREn). Les interruptions émises lorsque des données peuvent être envoyées au périphérique ne sont pas nécessaires et peuvent donc être désactivées (bit KMITXINTREn).

Il est aussi requis d'enregistrer auprès du contrôleur d'interruptions de SO3 la routine de service qui sera exécutée à chaque fois que des données envoyées par le périphérique peuvent être lues par le driver. Ceci est fait grâce à la fonction \texttt{irq\_bind} définie dans \texttt{irq.c}.

L'initialisation est faite dans la fonction \texttt{pl050\_init} du fichier \texttt{pl050.c}.

\begin{lstlisting}[language=C,caption=Initialisation commune du driver du PL050]
int pl050_init(dev_t *dev, dev_t *dev_copy, struct devclass *cdev,
               irq_return_t (*isr)(int, void *))
{
	/* Set the clock divisor (arbitrary value). */
	iowrite8(dev->base + KMI_CLKDIV, 2);

	/* Enable transmitter and receiver interrupt. */
	iowrite8(dev->base+KMI_CR, KMICR_EN | KMICR_RXINTREN | KMICR_TXINTREN);

	/* Bind the ISR to the interrupt controller. */
	irq_bind(dev->irq, isr, NULL, NULL);

	/* Register the input device so it can be accessed from user space. */
	devclass_register(dev, cdev);

	return 0;
}
\end{lstlisting}

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/kmiregs.png}
    \caption{Registres du PL050 tels que décrits par la documentation ARM \cite{arm-pl050-main}}
\end{figure}

\subsubsection{Envoi de données}

Lorsque le driver souhaite envoyer des données au périphérique, il doit les écrire dans le registre KMIDATA (8 bits). Avant cela, le driver doit vérifier que ce registre soit vide, ceci est fait en s'assurant que le bit TXEMPTY du registre KMISTAT (\textit{status register}) est à 1. Cela a été codé dans la fonction \texttt{pl050\_write} du fichier \texttt{pl050.c}.

\begin{lstlisting}[language=C,caption=Écriture du registre KMIDATA du PL050]
void pl050_write(dev_t *dev, uint8_t data)
{
	/* Check if we can actually write in the transmit registry. */
	uint8_t status = ioread8(dev->base + KMI_STAT);
	if (0 == (status & KMISTAT_TXEMPTY)) {
		printk("%s: write timeout.\n", __func__);
		return;
	}

	iowrite8(dev->base + KMI_DATA, data);
}
\end{lstlisting}

\subsubsection{Réception de données}

Lorsque le périphérique écrit des données dans le registre KMIDATA, la routine de service du driver est exécutée puisqu'une interruption est levée. Dans cette routine, il faut lire le contenu du registre KMIDATA tant que le bit KMIRXINTR du registre KMIIR (\textit{interrupt status register}) est à 1.

Cela permet de regrouper les octets lus en un paquet PS2, lequel doit être analysé différemment selon qu'il provienne de la souris ou du clavier. La routine de service du clavier est la fonction \texttt{pl050\_int\_keyboard} du fichier \texttt{kmi0.c} et celle de la souris est la fonction \texttt{pl050\_int\_mouse} du fichier \texttt{kmi1.c}.

\begin{lstlisting}[language=C,caption=Récupération d'un paquet PS2 envoyé par le clavier]
irq_return_t pl050_int_keyboard(int irq, void *dummy)
{
	/* Read the interrupt status register. */
	uint8_t status, packet[2], i, tmp;
	status = ioread8(pl050_keyboard.base + KMI_IR);

	/* As long as a receiver interrupt has been asserted */
	i = 0;
	while (status & KMIIR_RXINTR) {
        tmp = ioread8(pl050_keyboard.base + KMI_DATA);
		if (i < 2) /* Remember only the first 2 bytes. */
			packet[i++] = tmp;

		/* Update status. */
		status = ioread8(pl050_keyboard.base + KMI_IR);
	}

	get_kb_key(packet, i, &last_key); /* defined in ps2.c */
	return IRQ_COMPLETED;
}
\end{lstlisting}

\subsubsection{Traitement des paquets}

\paragraph*{Souris} À chaque mouvement de la souris, celle-ci envoie une série de paquets contenant, entre autres, la différence de position de la souris (abscisse et ordonnée) par rapport au paquet précédent et l'état des boutons de la souris (s'ils ont été appuyés ou non). À partir de ces données, le driver calcule et garde en mémoire les coordonnées absolues de la souris ainsi que l'état de ses boutons. Ces données sont stockées dans une structure de type \texttt{ps2\_mouse}. La fonction remplissant ces données est \texttt{get\_mouse\_state} du fichier \texttt{ps2.c}.

\begin{lstlisting}[language=C,caption=Analyse d'un paquet PS2 envoyé par la souris]
void get_mouse_state(uint8_t *packet, struct ps2_mouse *state, uint16_t max_x, uint16_t max_y)
{
	if (!state->left)
		state->left = packet[PS2_STATE] & BL;
	
	if (!state->right)
		state->right = packet[PS2_STATE] & BR;

	if (!state->middle)
		state->middle = packet[PS2_STATE] & BM;

	/* Retrieve dx and dy values to compute the mouse coordinates. */
	state->x += GET_DX(packet[PS2_STATE], packet[PS2_X]);
	state->y -= GET_DY(packet[PS2_STATE], packet[PS2_Y]);

	state->x = CLAMP(state->x, 0, max_x);
	state->y = CLAMP(state->y, 0, max_y);
}
\end{lstlisting}

\paragraph*{Clavier} Le traitement des paquets provenant du clavier est bien plus complexe \cite{osdev-kbd}. En effet, il faut implémenter un mécanisme qui prenne en compte :

\begin{itemize}
    \item les différents types de touches (certaines envoient un paquet de 2 octets, d'autres de 3 octets),
    \item l'état de la touche concernée (appuyée ou relâchée),
    \item la présence ou non de touches modifiant le comportement d'une autre touche ; tout en sachant que ces touches (e.g. Shift, Ctrl, Alt) peuvent être combinées et qu'elles sont chacune envoyées dans leur propre paquet,
    \item les différents types de comportements lorsqu'une touche est appuyée (A doit afficher un « a », Shift + A un « A », alors que Ctrl + A devrait pouvoir exécuter une action définie par l'espace utilisateur).
\end{itemize}

Et tout ceci sans prendre en compte la langue du clavier qui pourrait être définie et modifiée par l'utilisateur. Ainsi, pour simplifier l'écriture de ce driver, seules les fonctionnalités essentielles suivantes ont été implémentées :

\begin{itemize}
    \item lettres minuscules,
    \item lettres majuscules (avec la touche Shift),
    \item quelques touches de contrôles (e.g. flèches, retour arrière, supprimer, tabulation).
\end{itemize}

Le driver traduit donc les \textit{scan codes} (code d'identification d'une touche) présents dans les paquets reçus en un caractère UTF-8 et garde en mémoire le dernier code calculé. Ce code est stocké dans une structure de type \texttt{ps2\_key}. Le traitement des paquets est fait dans la fonction \texttt{get\_kb\_state} du fichier \texttt{ps2.c}.

Afin de savoir comment traduire un scan code en un caractère UTF-8, plusieurs tableaux de traduction ont été codés dans \texttt{ps2.c}. Les éléments sont définis de la manière suivante : \texttt{tableau[scan code] = caractère UTF-8}. L'inconvénient de cette méthode est que tous les éléments des tableaux ne sont pas utilisés, il y a donc un gaspillage de mémoire.

\begin{lstlisting}[language=C,caption=Analyse d'un paquet PS2 envoyé par le clavier]
void get_kb_key(uint8_t *packet, uint8_t len, struct ps2_key *key)
{
	uint8_t i = 0;

	/* If the first byte is KEY_EXT then we will use the extended scan codes array. */
	key->state &= ~KEY_ST_EXTENDED;
	if (packet[i] == KEY_EXT) {
		key->state |= KEY_ST_EXTENDED;
		i++;
	}

	/* Change the pressed/released state bit. */
	key->state |= KEY_ST_PRESSED;
	if (packet[i] == KEY_REL) {
		key->state &= ~KEY_ST_PRESSED;
		i++;
	}

	/* Change the shift modifier bit (left or right shift key). */
	if (packet[i] == KEY_LSH || packet[i] == KEY_RSH) {
		key->value = 0;
		key->state ^= KEY_ST_SHIFT;
	}
	else {
		/* Convert the scan code to the wanted value. */
		if (key->state & KEY_ST_SHIFT)
			key->value = scs[packet[i]];
		else if (key->state & KEY_ST_EXTENDED)
			key->value = sce[packet[i]];
		else
			key->value = sc[packet[i]];
	}
}
\end{lstlisting}

\subsection{Coordonnées et touches appuyées}

Pour récupérer la position de la souris et les touches appuyées du clavier depuis l'espace utilisateur, il faut passer par l'appel système \texttt{ioctl} qui permet d'exécuter une opération définie par le driver. Au préalable, il est nécessaire d'obtenir un descripteur de fichier pour le driver correspondant, de la même manière que pour le framebuffer.

Les opérations \texttt{ioctl} disponibles sont définies dans les fichiers \texttt{kmi0.c} et \texttt{kmi1.c} et sont les suivantes.

\begin{itemize}
    \item \texttt{SET\_SIZE} : permet d'indiquer au driver la résolution de l'écran, ainsi il peut poser une limite maximale aux coordonnées de la souris (afin que celle-ci ne sorte pas de l'écran). C'est une structure de type \texttt{display\_res} qu'il faut passer en paramètre à l'opération.
    \item \texttt{GET\_STATE} : permet de récupérer les coordonnées de la position actuelle de la souris sous la forme d'une structure de type \texttt{ps2\_mouse}.
    \item \texttt{GET\_KEY} : permet de récupérer la valeur de la dernière touche appuyée du clavier. La structure retournée est de type \texttt{ps2\_key}.
\end{itemize}
