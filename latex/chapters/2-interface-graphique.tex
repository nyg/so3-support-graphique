\chapter{Interface graphique}
\label{ch:interface-graphique}

\section{Choix de LittlevGL}

Le choix de la librairie graphique s'est porté sur LittlevGL, librairie open-source écrite en C et sous licence MIT \cite{lvgl}. Elle a été conçue spécifiquement pour les systèmes embarqués et par conséquent ne requiert que peu de ressources. La librairie permet aussi la copie des pixels de l'interface graphique dans un framebuffer. Ce dernier étant une portion de la mémoire vive dans laquelle s'écrit une représentation binaire d'un ensemble de pixels afin de d'être affichés à l'écran.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.75\columnwidth]{images/lvgl-example.png}
    \caption{Exemple d'interface graphique tel que présenté sur le site web de LittlevGL}
\end{figure}

Initialement, cette librairie ne devait être utilisée que dans un premier temps pour faciliter l'intégration d'une autre librairie plus complète (telles que Qt ou GTK). Cependant, au vu des nombreuses fonctionnalités offertes par LittlevGL, de sa documentation très complète et d'une communauté active, il a été décidé de garder cette librairie jusqu'à la fin du travail de Bachelor.

Avec la sortie de la version 7 en mai 2020, le projet est renommé en LVGL (\textit{Light and Versatile Graphic Library}). Cependant, l'appellation LittlevGL est gardée dans ce rapport car c'est la version 6.1.2 (février 2020) qui a été intégrée à SO3.

\section{Intégration et configuration}

La librairie est composée d'un répertoire nommé \texttt{lvgl} contenant son code source ainsi que plusieurs fichiers Makefile indiquant le chemin des sources devant être compilées. Il n'existe cependant pas de Makefile compilant la libraire.

Un fichier de configuration \texttt{lv\_conf.h} est aussi fourni et doit être placé au même niveau que le répertoire \texttt{lvgl}. Il contient plusieurs paramètres tels que la résolution maximale que doit supporter la librairie, le nombre de bits par pixel, le niveau de log à afficher, etc.

La résolution maximale a été fixée à $1920 \times 1080$ ce qui est suffisant dans notre contexte de système embarqué. Quant à la résolution effective — qui est limitée par celle de l'écran utilisé — elle est communiquée à LittlevGL par l'application utilisateur. Un autre paramètre modifié est le nombre de bits par pixels, fixé à 32 — un octet pour chacune de ces valeurs : la transparence, le rouge, le vert et le bleu.

La librairie a été placée dans l'espace utilisateur de SO3 et non pas dans le noyau, car ce sont les applications utilisateur qui vont en faire usage. Elle a été placée dans le répertoire \texttt{so3/usr/lvgl}. L'arborescence est donc la suivante :

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.29\columnwidth]{images/lvgl-arb.png}
    \caption{Arborescence de \texttt{so3/usr/}}
\end{figure}

\section{Compilation}

Il a fallu écrire le fichier \texttt{lvgl/Makefile} afin que celui-ci produise une librairie statique pouvant être liée à l'exécutable de chacune des applications écrites dans \texttt{usr/src/}.

Le contenu de ce Makefile reste relativement simple, la cible \texttt{all} s'occupe de compiler tous les fichiers C — lesquelles sont indiqués par les fichiers Makefile (\texttt{*.mk}) susmentionnés — et de regrouper les fichiers objets résultants dans l'archive \texttt{liblvgl.a} avec la commande \texttt{ar}.

Il s'agissait ensuite de modifier le Makefile de \texttt{usr/} afin de lier la librairie au fichier objet de chaque application. Pour cela, une nouvelle cible créant la librairie a été écrite, puis celle-ci a été rajoutée comme dépendance de la cible créant l'exécutable (fichier ELF) de chacune des applications.

\begin{lstlisting}[language=make,caption=\texttt{usr/Makefile} : ajout de la cible compilant la librairie]
out/%.elf: src/%.o $(LIBC_DIR)/$(NLIB) $(LVGL_DIR)/$(LVGL_LIB)
    $(LD) -o $@ libc/crt0.o libc/crt1.o $< \
          -L$(LIBC_DIR) -lso3 \
          -L$(LVGL_DIR) -llvgl $(LDFLAGS)

$(LVGL_DIR)/$(LVGL_LIB):
	make -C lvgl all
\end{lstlisting}

Ainsi, une application utilisateur de SO3 peut désormais utiliser les fonctions définies par LittlevGL simplement en incluant le fichier \texttt{lvgl/lvgl.h}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{images/compil-so3-usr.pdf}
    \caption{Vue générale de la compilation d'une application SO3}
\end{figure}

\section{Utilisation et fonctionnement}

Toute application voulant utiliser LittlevGL doit appeler certaines fonctions et initialiser certaines structures de la librairie. Connaître ces différents éléments permet de mieux définir et comprendre les modifications qui devront être apportées à SO3. L'extrait suivant montre de manière simplifiée un exemple d'application LittlevGL (un exemple complet et fonctionnel a été écrit dans \texttt{usr/src/demo.c}).

\begin{lstlisting}[language=C,caption=Exemple d'application LittlevGL]
#include "lvgl/lvgl.h"

int main(int argc, char **argv)
{
	lv_init(); /* Internal LittlevGL initialization. */

	fs_init(); /* User-defined initializations. */
	fbdev_init();
	inputdev_init();
	
	create_ui();

	pthread_t thread; /* LittlevGL must know how time passes by. */
	pthread_create(&thread, NULL, tick_routine, NULL);
	
	while (1) { /* Run LittlevGL-defined tasks. */
		lv_task_handler();
		usleep(5000);
	}

	return 0;
}

void *tick_routine(void *args)
{
	while (1) { /* Tell LittlevGL that 5ms passed. */
		usleep(5000);
		lv_tick_inc(5);
	}
}
\end{lstlisting}

LittlevGL initialise plusieurs de ses modules grâce à la fonction \texttt{lv\_init}, son appel est donc obligatoire \cite{lvgl-init}. Les fonctions d'initialisation \texttt{*\_init} qui suivent sont définies par le développeur et doivent initialiser certaines structures de LittlevGL. Ces structures seront vues plus en détails par la suite.

Deux autres autres composants sont essentiels, le premier est un thread qui indique à intervalle régulier le temps écoulé entre deux appels de la fonction \texttt{lv\_tick\_inc} \cite{lvgl-tick} (fonction définie par LittlevGL). Cela permet à la librairie de savoir quand exécuter certaines animations et tâches (e.g. récupération des coordonnées de la souris). Le deuxième est l'appel toutes les 5 millisecondes (valeur recommandée par LittlevGL) de \texttt{lv\_task\_handler}. Cette fonction s'occupe d'exécuter des tâches définies soit par la librairie (e.g. rafraîchissement de l'interface si changements) soit par le développeur (ce n'est pas le cas ici) \cite{lvgl-task}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{images/lvgl-app.pdf}
    \caption{Vue générale d'une application LittlevGL et de son interaction avec SO3}
\end{figure}

\subsection{Affichage de l'interface}

\subsubsection{Description et utilisation du framebuffer}

Afin d'afficher l'interface graphique générée par LittlevGL à l'écran, il faut pouvoir accéder et écrire dans le framebuffer depuis l'espace l'utilisateur de SO3.

Le framebuffer et un espace mémoire \cite{fb-wiki} dans lequel est écrit un ensemble de pixels. Il existe aussi un contrôleur de périphérique (circuit électronique) dédié au framebuffer et permettant de le configurer (par exemple pour décider de comment est représenté un pixel dans la mémoire — nombre de bits par couleur, ordre des couleurs, etc.).

Par exemple, avec 24 bits par pixel (\textit{bpp} — 8 pour chaque couleur rouge, vert et bleu) il est possible de représenter plus de 16 millions de couleurs. Chaque pixel est alors représenté sur quatre octets, le premier est soit ignoré soit utilisé pour l'opacité, les suivants sont interprétés différemment selon la configuration du contrôleur en RGB ou BGR (inversion du rouge et du bleu).

Afin de communiquer avec ce contrôleur, il est nécessaire d'écrire un driver qui ne se situe pas dans l'espace utilisateur mais dans le noyau. Un driver est écrit pour un contrôleur spécifique et chaque système embarqué possède son propre ensemble de contrôleurs (à moins que des contrôleurs existants soit réutilisés). C'est donc avec le driver du framebuffer qu'il faut communiquer depuis l'espace utilisateur. 

Dans le noyau Linux ainsi que dans les autres systèmes d'exploitation s'inspirant d'Unix, il existe des fichiers spéciaux \cite{devfile-wiki} (ou fichiers de périphérique) qui n'existent pas sur le système de fichiers (e.g. FAT, NTFS) mais lesquels donnent accès à un périphérique. Par exemple, un framebuffer est représenté par un fichier nommé \texttt{/dev/fbX}, \texttt{X} étant un identifiant numérique. Cette technique est intéressante car elle permet aussi de gérer d'autres périphériques tels que ceux d'entrée comme le clavier et la souris, avec des noms de fichier comme \texttt{/dev/keyboardX} ou \texttt{/dev/mouseX}.

Bien que SO3 puisse déjà ouvrir des fichiers normaux, les fichiers spéciaux ne sont pas gérés et il a été décidé de les implémenter. L'implémentation est décrite dans la section \ref{sec:specfile}.

Dans Linux, une fois le fichier du framebuffer ouvert et le descripteur de fichier récupérer (grâce à l'appel système \texttt{open}), une écriture dans ce fichier correspondra à une écriture dans le framebuffer. Cependant, utiliser l'appel système \texttt{write} de manière répétitive impactera les performances de l'application.

Une technique plus performante consiste à rendre accessible l'espace mémoire dédié au framebuffer dans l'espace utilisateur. Cela passe par l'appel système \texttt{mmap} et il n'est requis qu'une seule fois. Un pointeur sur l'espace mémoire est alors récupéré et le framebuffer peut être écrit. Cet appel système n'existe pas non plus dans SO3 et doit donc être implémenté.

Concrètement, voici le code nécessaire afin d'écrire un pixel dans le framebuffer. Ce système est aussi ce qui permettra de copier l'interface graphique de LittlevGL dans le framebuffer.

\begin{lstlisting}[language=C,caption={Exemple d'écriture dans un framebuffer}]
#include <fcntl.h>
#include <sys/mman.h>

int fd = open("/dev/fb0", O_WRONLY); /* Open the framebuffer. */
int *fbp = mmap(NULL, FB_SIZE, 0, 0, fd, 0); /* Map the memory. */
fbp[0] = 0x00ff0000; /* Write a red pixel in 24bpp RGB. */
\end{lstlisting}

\subsubsection{Driver d'affichage LittlevGL}

Dans la nomenclature de la librairie, un \textit{driver} correspond a une structure C permettant d'activer une fonctionnalité. À ces drivers sont attachées une ou plusieurs fonctions \textit{callback} qui sont appelées par la librairie au moment propice.

Par exemple, au driver d'affichage \cite{lvgl-disp} (\textit{display driver}) il faut associer un buffer que LittlevGL utilisera pour redessiner l'interface. Un callback doit aussi être renseigné lequel copie ce buffer dans le framebuffer. Si ce buffer est plus petit que l'écran, l'interface sera redessinée en plusieurs fois. Il y a donc un compromis à faire entre utilisation de la mémoire et utilisation du CPU. À noter que LittlevGL ne va redessiner que les parties de l'interface qui ont changées.

Il est aussi possible de fournir à LittlevGL un deuxième buffer. Dans ce cas, les opérations de rafraîchissement de l'interface et de copie dans le framebuffer peuvent être faites en parallèle. Il faut cependant que l'OS supporte l'accès direct à la mémoire (\textit{Direct Memory Access}) ce qui n'est pas le cas de SO3, ce deuxième buffer n'a donc pas été utilisé dans notre cas.

Le code source suivant montre comment doit être initialisé le driver ainsi que le fonctionnement du callback qui utilise le pointeur sur le framebuffer récupéré précédemment.

\begin{lstlisting}[language=c,caption=LittlevGL : initialisation du driver d'affichage]
static lv_color_t buf[LVGL_BUF_SIZE]; /* Buffer initialization. */
static lv_disp_buf_t disp_buf;
lv_disp_buf_init(&disp_buf, buf, NULL, LVGL_BUF_SIZE);

static lv_disp_drv_t disp_drv; /* Driver initialization. */
lv_disp_drv_init(&disp_drv);
disp_drv.hor_res = scr_hres;
disp_drv.ver_res = scr_vres;
disp_drv.buffer = &disp_buf;
disp_drv.flush_cb = my_fb_cb;
lv_disp_drv_register(&disp_drv);
\end{lstlisting}

\begin{lstlisting}[language=C,caption=Fonction callback de copie du buffer LittlevGL dans le framebuffer]
void my_fb_cb(lv_disp_drv_t *disp,
              const lv_area_t *area,
              lv_color_t *pixel)
{
	lv_coord_t y, line_width = lv_area_get_width(area);
	uint32_t line_size = line_width * sizeof(lv_color_t);
	
	for (y = area->y1; y <= area->y2; y++) { /* Copy line-by-line. */
		memcpy(&fbp[y * scr_hres + area->x1], pixel, line_size);
		pixel += line_width;
	}

	lv_disp_flush_ready(disp);
}
\end{lstlisting}

\subsection{Gestion des périphériques d'entrée}

Un accès au clavier et à la souris peut être obtenu de la même manière que cela a été expliqué pour le framebuffer. Dans SO3, les drivers pour le clavier et la souris n'existent pas, ils doivent donc être implémentés.

Une fois le descripteur de fichier obtenu, une opération de lecture ou d'écriture n'aurait que peu de sens à moins de définir un protocole d'échange de données (e.g. récupération des coordonnées de la souris). Les OS inspirés d'Unix ont mis en place un appel système nommé \texttt{ioctl} \cite{ioctl-wk} permettant d'exécuter des opérations définies par un driver.

Cet appel système existe déjà dans SO3, seules les opérations nécessaires doivent être implémentées. Pour le clavier, seule une opération pour récupérer la dernière touche pressée est requise. Pour la souris, deux opérations doivent être écrites. La première pour récupérer les coordonnées de la souris et la deuxième afin d'indiquer au driver la résolution de l'écran. Cette information est nécessaire afin que le driver puisse limiter les coordonnées de la souris, afin d'empêcher celle-ci de « sortir » de l'écran.

\subsubsection{Driver d'entrée – Souris}

L'initialisation du driver d'entrée LittlevGL \cite{lvgl-input} pour la souris et similaire à celle du framebuffer. La fonction callback exécutera l'opération pour récupérer les coordonnées de la souris et l'état de ses boutons. L'opération pour indiquer la résolution de l'écran doit être exécutée après l'obtention du descripteur de fichier de la souris.

\begin{lstlisting}[language=C,caption=LittlevGL : initialisation du driver pour la souris]
int mfd = open("/dev/mouse0", 0);

struct display_res res = { .h = scr_hres, .v = scr_vres };
ioctl(mfd, IOCTL_MOUSE_SET_RES, &res);

lv_indev_drv_t mouse_drv; /* Initialisation du driver. */
lv_indev_drv_init(&mouse_drv);
mouse_drv.type = LV_INDEV_TYPE_POINTER;
mouse_drv.read_cb = my_mouse_cb;
\end{lstlisting}

\begin{lstlisting}[language=C,caption=Fonction callback pour récupérer les coordonnées de la souris]
bool my_mouse_cb(lv_indev_drv_t *indev, lv_indev_data_t *data)
{
	static struct ps2_mouse mouse;
	ioctl(mfd, IOCTL_MOUSE_GET_STATE, &mouse);

	data->state = mouse->left ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
	data->point.x = mouse->x;
	data->point.y = mouse->y;

	return false;
}
\end{lstlisting}

\subsubsection{Driver d'entrée – Clavier}

L'initialisation du driver LittlevGL pour le clavier suis exactement le même principe. La fonction callback s'occupe par contre de récupérer la dernière touche appuyée du clavier grâce à un l'appel \texttt{ioctl}.

\begin{lstlisting}[language=C,caption=Fonction callback pour récupérer la dernière touche appuyée du clavier]
bool my_keyboard_cb(lv_indev_drv_t *indev, lv_indev_data_t *data)
{
	static struct ps2_key key;
	ioctl(kfd, IOCTL_KB_GET_KEY, &key);

	if (key.value != 0) {
		data->key = key.value;
		data->state = key.state & 1;
	}

	return false;
}
\end{lstlisting}

\subsection{Accès au système de fichiers}

LittlevGL fournit aussi un driver \cite{lvgl-fs} pour accéder à des systèmes de fichiers de l'OS. Cela permet par exemple d'afficher des images dans l'interface graphique. Il faut initialiser un driver de ce type pour chaque système de fichiers devant être accéder par LittlevGL. De manière similaire à ce qui est fait sous Windows, une lettre est associée à chaque instance de driver permettant ainsi d'identifier les fichiers (e.g. \texttt{S:/mon-image.png}). SO3 ne possédant qu'un seul système de fichiers, seul un driver est nécessaire.

Les extraits suivants montrent le code nécessaire afin d'afficher une image dans l'interface graphique. Comme pour les drivers vus précédemment, plusieurs fonctions callback sont utilisées. Ici, elles servent à faire le lien entre LittlevGL et les fonctions C existantes de manipulation de fichier (e.g. \texttt{fopen}, \texttt{fread}, \texttt{fclose}). Dans SO3, ces fonctions sont fournies par musl \cite{musl}, une librairie qui implémente la librairie C standard.

\begin{lstlisting}[language=C,caption=LittlevGL : initialisation d'un driver de système de fichiers]
lv_fs_drv_t drv;
lv_fs_drv_init(&drv);

drv.letter = 'S';
drv.file_size = sizeof(FILE*);
drv.rddir_size = sizeof(FILE*);
drv.ready_cb = fs_ready_cb;		/* Drive is ready to use. */
drv.open_cb = fs_open_cb;		/* Callback to open a file. */
drv.close_cb = fs_close_cb;		/* Callback to close a file. */
drv.read_cb = fs_read_cb;		/* Callback to read a file. */
drv.seek_cb = fs_seek_cb;		/* Callback to set file cursor. */
drv.tell_cb = fs_tell_cb;		/* Callback to get file cursor. */
lv_fs_drv_register(&drv);
\end{lstlisting}

\begin{lstlisting}[language=C,caption=Exemples de callbacks pour l'ouverture et la lecture de fichiers]
lv_fs_res_t fs_open_cb(struct _lv_fs_drv_t *drv,
                       void *file_p,
                       const char *path,
                       lv_fs_mode_t mode)
{
	FILE *fp = fopen(path, (mode & LV_FS_MODE_WR) ? "w" : "r");
	if (!fp) {
		return LV_FS_RES_UNKNOWN;
	}

	*((FILE **)file_p) = fp;
	return LV_FS_RES_OK;
}

lv_fs_res_t fs_read_cb(struct _lv_fs_drv_t *drv,
                       void *file_p,
                       void *buf,
                       uint32_t btr, /* bytes to read */
                       uint32_t *br) /* bytes read */
{
	*br = fread(buf, sizeof(uint8_t), btr, *(FILE **)file_p);
	return LV_FS_RES_OK;
}
\end{lstlisting}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.75\columnwidth]{images/lvgl-app-detail.pdf}
    \caption{Vue générale des drivers LittlevGL et de leurs interactions avec SO3}
    \label{fig:lvgl-app-detail}
\end{figure}

La figure \ref{fig:lvgl-app-detail} illustre les interactions des drivers LittlevGL vu précédemment avec le noyau SO3. Les drivers de SO3 ainsi que le \textit{Virtual Filesystem} seront abordés dans le chapitre suivant.

\subsection{Application de démonstration}

Les extraits de code précédents font partie d'une application de démonstration écrite afin de démontrer les fonctionnalités de LittlevGL mais aussi, et surtout, de pouvoir s'assurer du bon fonctionnement des modifications qui seront apportées à SO3. Cette application se trouve dans le fichier \texttt{usr/src/demo.c}.

L'interface graphique de l'application (figures \ref{fig:demo-1} et \ref{fig:demo-2}), composée de trois onglets, est basé sur du code de démonstration de LittlevGL disponible sur leur dépôt Github \cite{lvgl-demo}. Le reste de l'application a été développé spécifiquement pour SO3.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\columnwidth]{images/demo-1.png}
    \caption{Premier onglet de l'application de démonstration avec affichage d'une image}
    \label{fig:demo-1}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\columnwidth]{images/demo-2.png}
    \caption{Deuxième onglet de l'application de démonstration avec un champ texte}
    \label{fig:demo-2}
\end{figure}
