\section{Portage sur Raspberry Pi 4}

\subsection{Introduction}

Jusqu'à présent, autant SO3 que SOO ont été testés sur la plateforme ARM Versatile Express émulée par QEMU. Cela a été fait pour faciliter et accélérer le développement mais cette situation ne correspond pas à la réalité d'une mise en production de SOO.

Le déploiement de SOO sur la Raspberry Pi 4 \cite{rasp} est un objectif de ce travail de Bachelor. Ce déploiement permet de tester le travail réalisé dans de vraies conditions et en utilisant un périphérique d'entrée comme un écran tactile sur lequel est aussi affiché l'interface graphique.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\columnwidth]{images/rpi-4-hardware.jpg}
    \caption{Composants de la Raspberry Pi 4 \cite{rasp-hw}}
    \label{fig:vinput-ring}
\end{figure}

\subsection{Écrans tactiles}

Trois écrans tactiles ont été achetés par le REDS afin de pouvoir les tester sur la Pi 4 et aussi de comparer les différences entre ces écrans, notamment la taille et résolution mais aussi la manière dont ils se connectent à la Pi 4 (e.g. HDMI, DSI). Après recherche et comparaison de multiples écrans, ceux choisis sont :

\begin{enumerate}
    \item l'écran officiel de la \href{https://www.raspberrypi.org/products/raspberry-pi-touch-display/}{fondation Raspberry Pi} d'une taille de 7" et d'une résolution de $800 \times 480$,
    \item un écran de la compagnie \href{https://www.52pi.com/7-inch-lcds/45-52pi-7-inch-1024x600-capacitive-touch-screen-hdmi-tft-lcd-display-for-raspberry-pibeagle-bone-blackwindows-10macbook-pro.html}{52Pi} d'une taille de 7" aussi mais d'une résolution plus élevée de $1024 \times 600$,
    \item un écran de la compagnie \href{https://www.waveshare.com/10.1inch-HDMI-LCD-B-with-case.htm}{Waveshare} d'une taille de 10.1" et d'une résolution de $1280 \times 800$.
\end{enumerate}

Le premier écran se connecte à la Pi 4 sur son port DSI. L'alimentation de l'écran peut être faite grâce au port micro-USB ou via les ports GPIO. Les événements d'entrée sont transférés via le port DSI.

Le deux derniers écrans se connectent sur le port micro-HDMI de la Pi 4. Leur alimentation ne peut être faite que par un port micro-USB présent sur chacun des écrans.

\subsection{Gestion des événements tactiles}

\subsubsection{Fonctionnement avec Raspbian}

Avec l'OS Raspbian — distribution officiel de la fondation Raspberry Pi basée sur Debian — les trois écrans sont reconnus automatiquement. Aucune configuration de l'OS n'est nécessaire.

\subsubsection{Fonctionnement avec SOO}

Les écrans n'ont pas fonctionné du premier coup lorsque la Pi 4 démarrait avec SOO. Le premier problème est qu'ils ne s'allumaient pas, bien que le branchement fut le même. La raison était que les ports USB n'étaient pas détectés par SOO et que, par conséquent, les écrans n'étaient pas alimentés.

Plusieurs solutions ont été testées mais sans succès. D'abord, différents drivers ont été activés dans le noyau Linux, notamment ceux relatifs aux contrôleurs USB, xHCI et PCIe. Puis, les fichiers DTS de SOO pour la Pi 4 ont été comparés avec ceux de la dernière version du noyau Linux, lequel offre un meilleur support de la Pi 4. Les fichiers DTS les plus récents n'ont pas pu être importés tels quels car ceux de SOO comportent plusieurs modifications relatives à SOO. Les fichiers ont alors été comparés un à un et les modifications qui auraient pu avoir un lien avec les ports USB ont été intégrée à SOO, notamment un bloc relatif au bus PCIe, ce dernier étant utilisé par les ports USB.

Après une recherche plus approfondie du Prof. Daniel Rossier, il semblerait que les adresses utilisées par le bus PCIe soient sur 64 bits alors que l'OS est compilé en 32 bits. Il faudrait donc que SOO soit compilé avec l'option LPAE (\textit{Large Physical Address Extensions}) pour que les ports USB fonctionnent.

Le non support des ports USB par SOO a amené un deuxième problème concernant les événements d'entrée des écrans tactile. En effet, alors que les câbles USB alimentant les écrans peuvent être branchés — à des fins de test — sur un ordinateur à proximité, les événements devant être transmis via USB ne peuvent alors pas être reçu par SOO. Une souris ou un clavier ne peuvent pas non plus être branchés.

Par conséquent, sur la Pi 4, les différents périphériques d'entrée n'ont pas été testés lors de ce travail de Bachelor.

\subsubsection{Implémentation du support des événements tactiles}

Malgré l'impossibilité de tester les événements tactiles, il a quand été décider d'ajouter un support basique de ceux-ci. Ces événements sont aussi transmis par les différents drivers à la fonction \texttt{input\_event} mentionnée précédemment et se composent donc aussi des trois valeurs \texttt{type}, \texttt{code} et \texttt{value}.

\paragraph*{Événements absolus}

Le support de deux types d'événements envoyés par l'écran a été décidé. Le premier concerne les événements de type \texttt{EV\_ABS} permettant de déplacer le curseur à l'écran. Contrairement à la souris, les événements possèdent une abscisse et une ordonnées. Par contre, ces coordonnées ne sont pas relatives à la résolution de l'écran. Lorsque l'écran tactile et connecté à la Pi 4, il communique au driver une valeur minimale et maximale pour chacun des deux axes et les coordonnées y sont relatives.

Cette valeur peut être récupérée dans la fonction \texttt{input\_register\_device} du fichier \texttt{input.c}. Depuis cette fonction est appelée une nouvelle fonction \texttt{vinput\_set\_touch\_bounds} créée dans le backend. Les valeurs minimales et maximales sont récupérées et enregistrées dans deux tableaux du frontend comme illustré dans les extraits de codes suivants.

\begin{lstlisting}[language=C,caption=\texttt{input.c} : envoi des valeurs minimales et maximales des écrans tactiles vers le backend]
int input_register_device(struct input_dev *dev)
{
    /* ... */
    
	if (test_bit(EV_ABS, dev->evbit) && dev->absinfo) {
		vinput_set_touch_bounds(dev->absinfo);
	}

    /* ... */
}
\end{lstlisting}

\begin{lstlisting}[language=C,caption=Backend vinput : enregistrement des valeurs minimales et maximales des écrans tactiles]
static int32_t min[] = {0, 0};
static int32_t max[] = {1000, 1000}; /* Test values. */

void vinput_set_touch_bounds(struct input_absinfo *info)
{
	min[0] = info[0].minimum; /* x */
	max[0] = info[0].maximum;
	min[1] = info[1].minimum; /* y */
	max[1] = info[1].maximum;
}
\end{lstlisting}

Ces valeurs ne sont pas transmises au frontend mais sont utilisées directement par le backend pour modifier les événements \texttt{EV\_ABS} afin de ramener les coordonnées à la résolution de l'écran. Cela est fait dans la fonction \texttt{vinput\_pass\_event} du backend. Ainsi, le frontend reçoit directement les coordonnées de l'événement et peut les transmettre telles quelles au driver virtuel qui modifiera ensuite la position du curseur.

\paragraph*{Événements « touch »}

L'écran émet aussi un événement lorsque l'utilisateur tape du doigt sur l'écran. Dans ce cas c'est un événement de type \texttt{EV\_KEY} avec le code \texttt{BTN\_TOUCH} qui est envoyé. Actuellement, un tel événement sera envoyé au driver du clavier par le frontend. Il faut cependant qu'il soit envoyé à celui de la souris afin de simuler un clique avec le bouton gauche de la souris. La condition envoyant les événements au driver de la souris et modifiée comme suit :

\begin{lstlisting}[language=C,caption=Envoi des événements « touch » au driver de la souris]
if (type == EV_REL || /* is mouse movement */
   (type == EV_KEY && /* is mouse button click */
	(code == BTN_LEFT || code == BTN_MIDDLE || code == BTN_RIGHT || code == BTN_TOUCH)) ||
   (type == EV_ABS && /* is touchscreen event */
	(code == ABS_X || code == ABS_Y))) {

	so3virt_mse_event(type, code, value);
}
\end{lstlisting}

\subsection{Gestion des framebuffers}

\subsubsection{Détection de la résolution}

Avec Raspbian, la résolution des écrans est détectée automatiquement. Avec SOO, il est nécessaire de communiquer cette résolution dans le fichier \texttt{config.txt}. Ce fichier agit comme le BIOS sur un ordinateur du bureau \cite{config}. Communiquer la résolution implique qu'il faille modifier le fichier lorsque l'on change d'écran.

L'extrait suivant montre la configuration nécessaire pour les deux derniers écrans. Pour l'écran officiel de Raspberry, aucune modification n'est nécessaire. En effet, les modifications suivantes ne concernent que les écrans HDMI.

\begin{lstlisting}[language=C,caption=Configuration du \texttt{config.txt} pour les écrans tactiles connectés via HDMI]
# 52Pi - 7"
hdmi_group=2
hdmi_mode=87
hdmi_cvt=1024 600 60

# Waveshare - 10.1"
hdmi_group=2
hdmi_mode=27
\end{lstlisting}

Un mode correspond à une résolution et une fréquence d'affichage. Si la résolution voulue n'existe pas, il faut créer un nouveau mode et indiquer la résolution ainsi que la fréquence manuellement avec la propriété \texttt{hdmi\_cvt}. C'est qui est fait pour l'écran 52Pi.

\subsubsection{Création du framebuffer}

Le framebuffer de la Pi 4 est géré par un processeur multimédia nommé \textit{VideoCore} et développé par Broadcom. La documentation sur ce processeur est extrêmement limitée, surtout pour la version 6 qui est très différente de la version 4 embarquée sur la Raspberry Pi 3 \cite{vc}.

Lorsque la résolution est correctement mise dans le \texttt{config.txt}, le framebuffer est correctement créé par Linux et est présent dans le tableau \texttt{registered\_fb}. Cependant, il a été remarqué que la fonction \texttt{fb\_set\_par} de la structure \texttt{fb\_info} n'était pas définie et donc qu'il était impossible de modifier l'adresse du framebuffer devant être affiché à l'écran.

Afin d'avoir plus d'information sur le driver utilisé pour le framebuffer du VideoCore, la fonction \texttt{dump\_stack} a été utilisée dans la fonction \texttt{register\_framebuffer} de \texttt{fbmem.c}. Elle permet d'afficher la trace d'appels ayant mené à cette fonction. Il a été découvert que le driver utilisé est un driver générique nommé \textit{Simple Framebuffer}. Il semblerait alors que l'adresse mémoire physique du framebuffer est déterminée par le VideoCore et ne puisse pas être changée.

Cela pose un problème car le système implémenté jusqu'à présent repose sur la précondition que l'adresse du framebuffer puisse être changée.

\subsubsection{Affichage des différents framebuffers – Adaptation}

La solution implémentée afin de palier cette différence fut d'implémenter un thread copiant périodiquement le framebuffer du domaine actif dans le framebuffer du VideoCore, mais seulement quand le domaine actif est une ME.

Par contre, cela aura pour effet d'effacer le framebuffer de l'agence. Il faut donc allouer un espace mémoire dédié au framebuffer de l'agence. Cet espace mémoire ne sera utilisé que pour sauvegarder et restaurer l'état du VideoCore lorsque l'agence est le domaine actif. Cette allocation est faite dans la fonction \texttt{vfb\_set\_agencyfb}. L'extrait suivant montre la création de la structure \texttt{vfb\_domfb} pour l'agence ainsi que l'allocation mémoire.

\begin{lstlisting}[language=C,caption=Allocation de l'espace mémoire pour le framebuffer de l'agence]
static int vfb_set_agencyfb(struct fb_info *info)
{
	struct vfb_domfb *fb;
	fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
	fb->id = 0;

#ifdef CONFIG_ARCH_VEXPRESS
	/* ... */
#endif

#ifdef CONFIG_ARCH_BCM2835 /* Pi 4 */
	fb->vaddr = vmalloc(FB_SIZE);

	vfb_set_domfb(fb);
	vfb_set_active_domfb(fb->id);

	/* Start the framebuffer copy thread. */
	BUG_ON(!kthread_run(kthread_domfb_cpy, NULL, "fb-copy-thread"));
#endif

	return 0;
}
\end{lstlisting}

La sauvegarde de l'état du VideoCore dans le framebuffer de l'agence ne doit être fait seulement lorsque l'on passe de l'agence à une ME (via le double Ctrl + A). Cette sauvegarde est donc faite dans la fonction \texttt{vfb\_set\_active\_domfb} grâce au code suivant :

\begin{lstlisting}[language=C,caption=Sauvegarde du framebuffer du VideoCore dans celui de l'agence]
void vfb_set_active_domfb(domid_t domid)
{
    /* ... */

    if (active_domfb == 0 && domid != 0) {
    	memcpy(registered_domfb[0]->vaddr, vfb_info->screen_base, FB_SIZE);
    }
}
\end{lstlisting}

Puis, la restauration du framebuffer de l'agence dans celui du VideoCore ce fait dans le thread de copie, mais seulement lorsque l'on passe d'une ME à l'agence.

\begin{lstlisting}[language=C,caption=Thread de copie périodique des framebuffers]
static int kthread_domfb_cpy(void *arg)
{
	void *addr_domfb, *addr_vc = vfb_info->screen_base;
	uint32_t previous_domfb = 0;

	while (true) {
		if (active_domfb != 0 || previous_domfb != 0) {
			previous_domfb = active_domfb;
			addr_domfb = registered_domfb[previous_domfb]->vaddr;
			memcpy(addr_vc, addr_domfb, FB_SIZE);
		}

		msleep(50);
	}

	return 0;
}
\end{lstlisting}

Même si cette solution n'est utilisée pour le moment que sur la Pi 4, son avantage est qu'elle pourrait aussi l'être pour un autre contrôleur qui ne permet pas le changement de l'adresse du framebuffer. Il est aussi tout à fait possible d'utiliser cette solution pour tous les contrôleurs de framebuffer même si dans certains cas les performances seront dégradées à cause du thread de copie. Cependant, cela permettrait de ne pas avoir à gérer plusieurs architecture comme c'est maintenant le cas.
