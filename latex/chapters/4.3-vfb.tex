\section{Gestion des framebuffers}

\subsection{Création du backend et frontend}

Le backend a été crée dans \texttt{linux/soo/drivers/vfbback/vfb.c} et le frontend dans \texttt{SOO.refso3/so3/soo/drivers/vfbfront/vfb.c}. Le nom vfb correspond à \textit{virtual framebuffer} et respecte la convention actuelle des autres backends et frontends (e.g. vuart, vdummy).

Afin que le backend et le frontend soient correctement compilés, un Makefile contenant le nom du fichier objet devant être obtenu a été créé dans les répertoires \texttt{vfbback} et \texttt{vfbfront}. Le nom du fichier étant \texttt{vfb.c}, par convention le nom du fichier objet sera \texttt{vfb.o}.

\begin{lstlisting}[language=make,caption=Makefile du backend et frontend vfb]
/* vfbback/Makefile */
obj-$(CONFIG_VFB_BACKEND) := vfb.o

/* vfbfront/Makefile */
obj-$(CONFIG_VFB_FRONTEND) := vfb.o
\end{lstlisting}

La variable \texttt{CONFIG\_VFB\_*} fait partie du système de configuration Kconfig utilisé par le noyau Linux et repris par SO3. Il permet d'activer ou de désactiver des composants du noyau (e.g. drivers). Désactiver implique que le composant n'est pas compilé et donc qu'un binaire plus petit est obtenu.

Il est alors possible de rendre la compilation du vfb optionnelle ce qui permettrait de continuer à utiliser des ME sans interface graphique. Il faut pour cela que les deux variables soient déclarées. Celle du backend doit l'être dans \texttt{soo/drivers/Kconfig} et celle du frontend dans \texttt{so3/soo/drivers/Kconfig}. L'extrait suivant illustre la déclaration de la variable du backend.

\begin{lstlisting}[language=make,caption=Déclaration de la variable Kconfig pour le backend vfb]
config VFB_BACKEND
	bool "vfb framebuffer backend driver"
	default N
	help
	  This driver enabled to deal with a virtualized interface for a framebuffer
	  in the Mobile Entity.
\end{lstlisting}

La configuration du noyau (Linux ou SO3) se fait avec la commande \texttt{make menuconfig} exécutée dans le répertoire contenant le Makefile principal. Cette commande fournit une interface pour activer ou désactiver les composants du noyau.

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/kconfig.png}
    \caption{Activation du vfb dans le menu Kconfig de l'agence}
\end{figure}

Puisque le backend et frontend sont considérés comme des drivers par le noyau, il est nécessaire que les fichiers DTS décrivant le matériel de la plateforme référencent ces deux composants. L'extrait suivant montre les nœuds à ajouter. Le backend est rajouter dans \texttt{vexpress-tz.dts} (Linux) et le frontend dans \texttt{so3virt.dts} (SO3).

\begin{lstlisting}[caption=Modification des fichiers DTS]
agency {
	backends {
		vfb {
			compatible = "vfb,backend";
			status = "ok";
		};
	}
}

ME {
	frontends {
		vfb {
			compatible = "vfb,frontend";
			status = "ok";
		};
	};
};
\end{lstlisting}

Le backend et frontend sont maintenant compilés et seront initialisés lors du démarrage de l'agence et de la ME. Le backend est toujours initialisé avant le frontend car il fait partie des drivers de l'agence. Ce n'est qu'une fois l'agence démarrée que la ME est à son tour lancée. Le backend et frontend vfb n'ont pas été écrits à partir de zéro mais sont basés sur ceux du vdummy, qui fournit une base fonctionnelle pour la création de drivers backend et frontend. Une fois le fichier \texttt{vdummy.c} copié, il suffit de remplacer toutes les occurrences de \textit{vdummy} par \textit{vfb}. Il ne faut pas oublier de faire de même pour les headers (\texttt{vdummy.h}).

\subsection{Emplacement du framebuffer}

Comme mentionné précédemment, il n'y a pas de notion de matériel dans une ME. C'est-à-dire que, bien que la plateforme ARM Versatile Express continue d'être utilisée, la ME n'a pas accès au contrôleur PL111, ni d'ailleurs à la VRAM mise à disposition par la plateforme.

Il est alors nécessaire de réfléchir à l'endroit en mémoire où une ME doit placer son framebuffer. Grâce à un mécanisme de SOO existant (les \textit{grant tables} ou tables d'octrois), il serait possible de donner accès à la VRAM à une ME afin qu'elle y écrive l'interface graphique dessinée par l'application LittlevGL.

Cependant, cette solution pose deux problèmes. Premièrement, il faudrait mettre en place un système qui n'autoriserait que la ME active à aller écrire dans ce framebuffer. Sinon, chaque ME irait continuellement récrire ce que les autres ME ont écrit dans le framebuffer.

Deuxièmement, ce système empêcherait à l'agence d'accéder au framebuffer de chaque ME, indépendamment du domaine actif. En effet, parallèlement à ce travail de Bachelor est mené celui de Tommy Girardi \cite{girardi}, lequel nécessite un accès simultané à tous les framebuffers des ME virtualisées. Cela afin d'afficher sur un même écran divisé en plusieurs parties le contenu de chaque framebuffer. De ce fait, la solution proposée plus haut ne peut pas être considérée.

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/multi-fb.pdf}
    \caption{Emplacement de la mémoire allouée pour le framebuffer : solution choisie}
\end{figure}

La solution choisie consiste à allouer un espace mémoire pour chaque framebuffer de ME. Cette allocation pourrait avoir lieu soit dans l'agence soit dans la ME. Même si, techniquement, les deux cas sont possibles, il a été décidé d'effectuer cette allocation dans la ME.

En effet, en allouant les espaces mémoire dans l'agence (c'est-à-dire par le noyau Linux), il devient plus difficile de protéger ces framebuffers, notamment d'écritures non désirées. Lorsque l'espace mémoire est alloué dans la ME, il est possible de définir l'accès donné à l'agence comme étant en lecture seule.

\subsection{Taille du framebuffer}

La taille du framebuffer dépend de la résolution de l'écran et du nombre de bits par pixel. Or, ces informations ne sont connues que par l'agence puisque c'est elle qui a accès au matériel. Il faut donc communiquer ces informations à la ME qui en a besoin afin d'allouer l'espace mémoire pour le framebuffer.

\subsubsection{Obtention des propriétés du framebuffer}

Le noyau Linux possède un grand nombre de drivers pour différents types de framebuffers (\texttt{drivers/video/fbdev}). Lorsque Linux détecte un contrôleur de framebuffer, il l'initialise et ensuite enregistre une référence du framebuffer dans le tableau \texttt{registered\_fb} du fichier \texttt{fb.h}. Le tableau contient des pointeurs sur la structure \texttt{fb\_info}. C'est une structure complexe qui est utilisée par Linux pour décrire et manipuler un framebuffer. Elle contient, entre autres, une structure de type \texttt{fb\_var\_screeninfo} qui contient notamment la résolution horizontale et verticale du framebuffer ainsi que le nombre de bits par pixel.

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{images/vfbinfo.pdf}
    \caption{Initialisation du backend vfb : recherche de la structure \texttt{fb\_info}}
\end{figure}

Pour la plateforme Versatile Express, c'est le driver \texttt{amba-clcd.c} qui est utilisé par Linux pour initialiser le PL111. Cependant, le driver ne connaît pas les valeurs à utiliser pour initialiser le framebuffer. Ces valeurs proviennent de la DTS de la plateforme. L'extrait suivant est un nœud du fichier \texttt{vexpress-v2m-rs1.dtsi} décrivant le contrôleur PL111.

\begin{lstlisting}[language=C,caption=Description du PL111 dans la DTS de la plateforme Versatile Express]
clcd@1f0000 {
	compatible = "arm,pl111", "arm,primecell";
	memory-region = <&vram>;

	panel {
		panel-timing {
			clock-frequency = <65000000>;
			hactive = <1024>;
			vactive = <768>;
			hsync-len = <136>;
			hfront-porch = <20>;
			hback-porch = <160>;
			vfront-porch = <3>;
			vback-porch = <29>;
			vsync-len = <6>;
		};
	};
};
\end{lstlisting}

On peut y distinguer la résolution voulue, $1024 \times 768$, ainsi qu'une référence à la VRAM qui est décrite dans la DTS \texttt{vepxress-tz.dts}. Le nombre de bits par pixel est calculé par le driver à partir de la propriété \texttt{max-memory-bandwidth} qui n'est pas définie dans l'extrait précédent. Le driver choisi alors la valeur de 32 bits par pixel, ce qui nous convient.

Il s'agit maintenant de choisir le bon framebuffer du tableau \texttt{registered\_fb}. Pour l'instant il n'est prévu d'avoir qu'un seul écran rattaché au système embarqué, choisir le premier framebuffer du tableau est une option suffisante. Cela est fait lors de l'initialisation du backend (\texttt{vfb\_init}). La première structure \texttt{fb\_info} non nulle de \texttt{registered\_fb} est choisie pour décider du framebuffer qui sera utilisé. Elle est stockée dans une variable propre au backend nommée \texttt{vfb\_info}. Une résolution minimale est cependant imposée afin d'exclure les framebuffers qui auraient une résolution trop petite. Celle-ci a été fixée arbitrairement à $640 \times 480$.

Le backend étant considéré comme un driver Linux au même titre que ceux des framebuffers, il n'est pas possible de garantir que le driver du framebuffer sera initialisé avant le backend, autrement dit que le tableau \texttt{registered\_fb} ait été rempli avant sa lecture par le backend. Cependant, Linux met un place un système de notifications afin d'être notifier d'événements relatifs aux framebuffers. Il faut utiliser la fonction \texttt{fb\_register\_client} définie dans \texttt{fb\_notify.c} et lui passer une structure de type \texttt{notifier\_block}. Celle-ci contient une fonction callback qui sera appelée à chaque événement relatifs aux framebuffers.

Linux définit plusieurs événements de ce type mais ceux voulus ne sont disponibles que pour la plateforme AM200 EPD. Il faut donc modifier le fichier \texttt{fb.h} afin que les événements \texttt{FB\_EVENT\_FB\_REGISTERED} et \texttt{FB\_EVENT\_FB\_UNREGISTERED} soient disponibles pour toutes les plateformes. Les fonctions \texttt{do\_register\_framebuffer} et \texttt{do\_unregister\_framebuffer} de \texttt{fbmem.c} doivent aussi être modifiées pour que les événements soient bien envoyés. Les modifications consistent simplement à enlever les \texttt{\#ifdef} utilisé par Linux pour restreindre le code à la plateforme AM200 EPD.

À noter qu'il n'est pas nécessaire de recevoir les événements relatif aux framebuffers si un framebuffer est détecté lors de l'initialisation du backend.

\begin{lstlisting}[language=C,caption=Fonction d'initialisation du backend vfb]
int vfb_init(void)
{
	/* We check if there's already a registered framebuffer device. */
	int i = 0;
	while (i < FB_MAX && vfb_set_agencyfb(registered_fb[i]))
		i++;

	/* Otherwise, we register a client that will be notified when a
	 * framebuffer is registered. */
	if (i == FB_MAX)
		fb_register_client(&vfb_fb_notif);

	return 0;
}
\end{lstlisting}

\subsubsection{Transmission des propriétés à la ME}

Pour transmettre la résolution du framebuffer aux ME, il a été décidé d'utiliser VBStore et donc de créer un chemin dans lequel est créé une propriété pour la résolution horizontale et une autre pour la résolution verticale. VBStore se prête bien à cette utilisation car la résolution consiste en deux valeurs à transmettre uniquement à l'initialisation de la ME. Il n'y a pas besoin de mettre en place un anneau partagé, système plus complexe, juste pour transmettre les propriétés du framebuffer.

Chaque backend possède son propre espace VBStore et doit être initialisé dans la fonction \texttt{vbstorage\_agency\_init} du fichier \texttt{vbstorage.c}. L'extrait suivant montre comment il est créé.

\begin{lstlisting}[language=C,caption=Création du chemin \texttt{/backend/vfb} dans l'agence]
np = of_find_compatible_node(NULL, NULL, "vfb,backend");
if (of_device_is_available(np))
	vbs_store_mkdir("/backend/vfb");
\end{lstlisting}

Dans cet extrait, la présence du nœud contenant une propriété avec la valeur \texttt{vfb,backend} est cherché dans la DTS et s'il est trouvé, le chemin \texttt{/backend/vfb} est créé. Un chemin est créé de la même manière pour chaque ME et chaque frontend mais cela se passe dans le fichier \texttt{vbstore\_me.c}. Une entrée doit être rajoutée pour le vfb comme le montre cet extrait de la fonction \texttt{vbstore\_devices\_populate}.

\begin{lstlisting}[language=C,caption=Création du chemin \texttt{/devices/<domain-id>/vfb/0} dans le frontend]
fdt_node = fdt_find_compatible_node("vfb,frontend");
if (fdt_device_is_available(fdt_node)) {
	DBG("%s: init vfb...\n", __func__);
	vbstore_dev_init(ME_domID(), "vfb", false, "vfb,frontend");
}
\end{lstlisting}

Cela crée un chemin ayant le format suivant \texttt{/devices/<domain-id>/vfb/0}. Puisque la résolution du framebuffer est commune à chaque ME, on pourrait l'écrire dans le VBStore du backend. Cependant, il a été choisi de l'écrire dans le VBStore de la ME car cela faciliterait un éventuel développement futur qui requerrait des résolutions différentes pour chaque ME.

Le chemin \texttt{devices/<domain-id>/vfb/0/resolution} est créé dans le backend avec la fonction \texttt{vbus\_mkdir} définie dans \texttt{vbus\_vbstore.c}. Puis, les deux propriétés \texttt{hor} et \texttt{ver} sont créées avec la fonction \texttt{vbus\_printf}. Le code suivante montre comment cela a été fait.

\begin{lstlisting}[language=C,caption=Création du chemin \texttt{resolution} et des propriétés \texttt{hor} et \texttt{ver}]
struct vbus_transaction vbt;
char dir[40];

vbus_transaction_start(&vbt);

sprintf(dir, "device/%01d/vfb/0", vdev->otherend_id);
vbus_mkdir(vbt, dir, "resolution");

sprintf(dir, "device/%01d/vfb/0/resolution", vdev->otherend_id);
vbus_printf(vbt, dir, "hor", "%u", FB_HRES);
vbus_printf(vbt, dir, "ver", "%u", FB_VRES);

vbus_transaction_end(vbt);
\end{lstlisting}

Dans la fonction \texttt{vfb\_probe} du frontend, la résolution est récupérée grâce à la fonction \texttt{vbus\_scanf} définie dans \texttt{vbus\_vbstore.c}.

\begin{lstlisting}[language=C,caption=Frontend vfb : récupération de la résolution stockée dans le VBStore]
struct vbus_transaction vbt;
uint32_t hres, vres;
char dir[40];

vbus_transaction_start(&vbt);
sprintf(dir, "device/%01d/vfb/0/resolution", ME_domID());
vbus_scanf(vbt, dir, "hor", "%u", &hres);
vbus_scanf(vbt, dir, "ver", "%u", &vres);
vbus_transaction_end(vbt);
\end{lstlisting}

\subsection{Réservation de l'espace mémoire}

Maintenant que la résolution est connue par le frontend, il est possible d'allouer l'espace mémoire pour le framebuffer puisque sa taille peut être calculée en multipliant la résolution horizontale, la résolution verticale et le nombre d'octet par pixel.

Il n'y a pas toujours une correspondance exacte entre le nombre de bits par pixel et le nombre d'octets par pixel. Par exemple, le PL111 définit le mode 24bpp mais un pixel sera quand même stocké sur 4 octets (un octet est ignoré). Dans Linux ainsi que LittlevGL, on parle plutôt de 32bpp (4 octets aussi). Le nombre de couleurs est le même (16 millions) mais au niveau logiciel le premier octet peut être utilisé pour la transparence.

Actuellement, le nombre d'octets par pixel a été fixé à quatre dans le backend. Si cela était amené à changer, il faudrait passer cette valeur au frontend via le VBStore comme pour la résolution.

Une fois la taille calculée, il est possible d'allouer l'espace mémoire pour le framebuffer. Cela est fait avec la fonction \texttt{get\_contig\_free\_pages} qui réserve un certain nombre de pages physiques. Ces pages n'appartiennent à aucun processus et ne seront pas libérées, cela est très proche du fonctionnement de la VRAM.

\begin{lstlisting}[language=C,caption=Frontend vfb : réservation de l'espace mémoire pour le framebuffer]
uint32_t fb_base, hres, vres, page_count;
char dir[40];

page_count = hres * vres * 4 / PAGE_SIZE;
fb_base = get_contig_free_pages(page_count);
BUG_ON(!fb_base);
so3virt_fb_set_info(fb_base, hres, vres);
\end{lstlisting}

\subsubsection{Création du driver virtuel}

Le driver virtuel vient remplacer dans SOO.refso3 le driver du PL111 décrit dans le chapitre \ref{ch:so3}. Il a été écrit dans le fichier \texttt{so3virt\_fb.c}. Comme pour les autres drivers, un nœud a du être rajouté dans la DTS \texttt{so3virt.dts}.

\begin{lstlisting}[caption=Description du driver virtuel framebuffer dans la DTS de SOO.refso3]
so3virt-fb {
	compatible = "fb,so3virt";
	status = "ok";
};
\end{lstlisting}

Il définit aussi les deux opérations sur fichier \texttt{mmap} et \texttt{ioctl} et dans sa fonction d'initialisation il enregistre une structure \texttt{classdev} afin que le fichier spécial \texttt{/dev/fb0} soit disponible dans l'espace utilisateur de la ME.

Les différences se trouvent dans la fonction d'initialisation, il n'y a pas de périphérique à initialiser et donc pas de registres à écrire. Dans la fonction \texttt{fb\_mmap}, l'adresse physique de la VRAM y est remplacée par celle réservée par le frontend. Le frontend communique cette adresse grâce à la fonction \texttt{so3virt\_fb\_set\_info} qui informe aussi le driver virtuel de la résolution de l'écran (nécessaire pour les opérations \texttt{ioctl}).

\subsection{Mapping du framebuffer dans l'agence}

Pour que l'espace mémoire réservé ci-dessus puisse être accédé par le backend, le frontend doit autoriser cet accès pour toutes les pages qui composent l'espace mémoire. Cela est fait juste après la réservation de l'espace mémoire par la fonction \texttt{gnttab\_grant\_foreign\_access} qui prend comme paramètres l'identifiant du domaine de l'agence et la page à autoriser.

Cette fonction est définie dans \texttt{gnttab.c} (\textit{grant table} ou table d'octrois) et fait partie d'un mécanise existant qui permet justement le partage de pages entre les différents domaines. La fonction retourne une référence vers l'entrée nouvellement créée dans la table d'octrois. C'est cette valeur qui est nécessaire au backend pour pouvoir accéder au framebuffer de la ME. La valeur étant un entier elle peut être écrite dans le chemin \texttt{device/\%01d/vfb/0/domfb-ref} de VBStore.

\begin{lstlisting}[language=C,caption=Frontend vfb : octroi de l'accès aux pages du framebuffer]
for (i = 0; i < page_count; i++) {
	res = gnttab_grant_foreign_access(vdev->otherend_id,
	                                  phys_to_pfn(fb_base + i * PAGE_SIZE),
	                                  1);
	BUG_ON(res < 0);

	if (i == 0) /* The back-end only needs the first grantref. */
		fb_ref = res;
}

sprintf(dir, "device/%01d/vfb/0/domfb-ref", ME_domID());
vbus_printf(vbt, dir, "value", "%u", fb_ref);
\end{lstlisting}

Ce chemin a été préalablement créé par le backend dans la fonction \texttt{vbf\_probe}. Le chemin est aussi observé par le backend afin qu'il soit notifié de toute modification de propriétés. Lorsqu'il y a une modification, la fonction \texttt{callback\_me\_domfb} est appelée.

\begin{lstlisting}[language=C,caption=Backend vfb : observation du chemin de la grantref]
watches[vdev->otherend_id] = kzalloc(sizeof(struct vbus_watch), GFP_ATOMIC);
vbus_watch_pathfmt(vdev,
                   watches[vdev->otherend_id],
                   callback_me_domfb,
                   "device/%01d/vfb/0/domfb-ref/value",
                   vdev->otherend_id);
\end{lstlisting}

C'est cette fonction qui alloue un espace mémoire dans l'agence et le mappe vers l'espace réservé par le frontend (partagé via la table d'octrois). Le code source suivant illustre ce procédé.

\begin{lstlisting}[language=C,caption=Frontend vfb : allocation d'un espace mémoire pour le framebuffer]
grant_ref_t fb_ref;
struct gnttab_map_grant_ref op;
struct vm_struct *area;
domid_t domid; /* ME domain id */

area = alloc_vm_area(FB_SIZE, NULL);

gnttab_set_map_op(&op,
                  (phys_addr_t) area->addr,
                  GNTMAP_host_map | GNTMAP_readonly,
                  fb_ref, domid, 0, FB_SIZE);

gnttab_map(&op);
\end{lstlisting}

\subsection{Affichage des différents framebuffers}

Comme expliqué dans la section \ref{sec:gst-multi-me}, il est possible de changer le domaine actif en tapant Ctrl + A deux fois. Avec les modifications apportées au fichier \texttt{console.c}, cette combinaison de touches appelle la fonction \texttt{vfb\_set\_active\_domfb} du backend.

Le but de cette fonction est de modifier les registres du PL111, notamment le registre LCDUPBASE afin d'y mettre l'adresse physique du framebuffer à afficher. Il est possible de faire cela en modifiant certaines valeurs de la structure \texttt{fb\_info} du framebuffer puis d'appeler l'opération \texttt{fb\_set\_par} qui va écrire les nouvelles valeurs de la structure dans les registres du contrôleur. L'appel de cette opération (définie dans la structure \texttt{fb\_info}) exécutera la fonction \texttt{clcdfb\_set\_par} puis \texttt{clcdfb\_set\_start}, laquelle modifie les registres voulus.

La valeur de \texttt{fb\_info} qui doit être modifiée est l'adresse physique de l'emplacement du framebuffer, soit \texttt{fb\_info->fix.smem\_start}. Par souci de cohérence, l'adresse virtuelle sera aussi modifiée, elle se trouve dans \texttt{fb\_info->screen\_base}.

Afin d'enregistrer les adresses physiques et virtuelles des différents framebuffers, la structure \texttt{vfb\_domfb} a été créée, elle représente un framebuffer de domaine. En effet, une telle structure ne sera pas seulement utilisée pour les ME mais aussi pour l'agence. Elle contient différentes valeurs :

\begin{itemize}
    \item \texttt{domid\_t id} : l'identifiant du domaine auquel appartient le framebuffer,
    \item \texttt{uint64\_t paddr} : l'adresse physique communiquée par le frontend,
    \item \texttt{char *vaddr} : l'adresse virtuelle du framebuffer,
    \item \texttt{struct vm\_struct *area} : un pointeur vers l'espace mémoire alloué par le backend,
    \item \texttt{grant\_handle\_t gnt\_handle} : l'identifiant du mapping effectué par le backend.
\end{itemize}

Les deux dernières valeurs sont utiles lorsqu'une ME est terminée, cela permet de dé-mapper l'espace alloué puis de le libérer. C'est la fonction \texttt{vfb\_close} du backend qui s'occupe de libérer ces ressources.

Il y a donc une structure \texttt{vfb\_domfb} pour chaque domaine devant afficher un framebuffer. Ces structures sont créées dans la fonction \texttt{callback\_me\_domfb} vue précédemment (à l'exception de la structure pour l'agence) et sont stockées dans le tableau \texttt{registered\_domfb}. L'indice du tableau correspond à l'identifiant du domaine. La taille du tableau correspond donc au nombre maximal de domaines pouvant être virtualisés. Ce nombre est repris du fichier \texttt{console.c} et est fixé à 8 (à noter que l'hyperviseur est inclus dans ce nombre même si ce n'est pas un domaine et qu'il ne possède pas de framebuffer).

\begin{lstlisting}[language=C,caption=Initialisation de la structure \texttt{vfb\_domfb} d'une ME]
struct gnttab_map_grant_ref op;
struct vm_struct *area;
struct vfb_domfb *fb;
domid_t domid;

fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
fb->id = domid;
fb->paddr = op.dev_bus_addr << 12;
fb->vaddr = area->addr;
fb->area = area;
fb->gnt_handle = op.handle;
vfb_set_domfb(fb); /* added to registered_domfb */
\end{lstlisting}

L'agence possède aussi sa structure \texttt{vfb\_domfb} car il est intéressant de pouvoir revenir sur le framebuffer de l'agence. Il est en effet possible que celui-ci ait été modifié par une autre application. La structure est créée dans la fonction \texttt{vfb\_set\_agencyfb} qui est appelée lorsque la structure \texttt{fb\_info} a été détectée, c'est-à-dire lors l'initialisation du backend ou lorsqu'un framebuffer est enregistré par Linux et que le backend en est notifié.

\begin{lstlisting}[language=C,caption=Initialisation de la structure \texttt{vfb\_domfb} pour l'agence]
static int vfb_set_agencyfb(struct fb_info *info)
{
	struct vfb_domfb *fb;

	if (vfb_info
	    || !info
	    || info->var.xres < MIN_FB_HRES
	    || info->var.yres < MIN_FB_VRES) {
		return -1;
	}

	vfb_info = info;

	fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
	fb->id = 0;
	fb->paddr = info->fix.smem_start;
	fb->vaddr = info->screen_base;

	vfb_set_domfb(fb);
	vfb_set_active_domfb(fb->id);

	return 0;
}
\end{lstlisting}

Il est nécessaire d'appeler \texttt{vfb\_set\_active\_domfb} car bien que le PL111 soit détecté par Linux et que la structure \texttt{fb\_info} soit initialisée, les registres du contrôleur n'ont pas été modifiés. L'appel est donc nécessaire pour que \texttt{clcdfb\_set\_par} soit appelé.

En activant le driver Linux \textit{Framebuffer Console}, le contrôleur sera activé et l'appel à \texttt{vfb\_set\_active\_domfb} ne sera pas nécessaire. Cependant, l'activation de ce driver pose problème car il crée un thread pour le clignotement du curseur de la console. Or, lorsque la structure \texttt{fb\_info} est modifiée (lors d'un changement du domaine actif) le thread ne peut plus écrire dans l'espace mémoire et fait donc planter le noyau Linux. Ce driver n'étant pas requis, il a été décidé de le considéré comme toujours désactivé.

\subsection{Notifications extérieures}

Une partie du travail de Bachelor de Tommy Girardi requiert d'être informée lorsque le framebuffer d'une ME est créé puis détruit (suite à la terminaison de la ME). Puisque l'adresse virtuelle du framebuffer est nécessaire, il a été décidé d'implémenter un simple système de callback.

La fonction \texttt{vfb\_set\_callback\_new\_domfb} permet de définir la fonction à appeler lorsqu'une nouvelle entrée est rajoutée au tableau \texttt{registered\_domfb}. La fonction doit avoir la signature suivante : \texttt{void (*cb)(struct vfb\_domfb *, struct fb\_info *)} et sera appelée à la fin de la fonction \texttt{callback\_me\_domfb}.

La fonction \texttt{vfb\_set\_callback\_rm\_domfb} définit la fonction devant être appelée lorsqu'une ME est terminée, son prototype est \texttt{void (*cb)(domid\_t id)} et elle sera appelée dans la fonction \texttt{vfb\_close} du backend.

\subsection{Terminaison d'une ME}

La terminaison d'une ME implique que plusieurs ressources doivent être libérées.

\begin{itemize}
    \item La structure \texttt{vbus\_watch} utilisée pour être notifié des modifications du chemin contenant la \texttt{grant\_ref} doit être libérée après avoir été desenregistrée.
    \item L'espace mémoire alloué par le backend doit être dé-mappé puis libéré (structure \texttt{vm\_struct}).
    \item L'entrée \texttt{vfb\_domfb} doit aussi être supprimée du tableau et libérée.
\end{itemize}

\begin{lstlisting}[language=C,caption=Backend vfb : libération des ressources lorsqu'une ME est terminée]
void vfb_close(struct vbus_device *vdev)
{
	struct gnttab_unmap_grant_ref op;

	/* Unregister and free the watch. */
	unregister_vbus_watch(watches[vdev->otherend_id]);
	kfree(watches[vdev->otherend_id]);
	watches[vdev->otherend_id] = NULL;

	/* Unmap the grantref. */
	gnttab_set_unmap_op(
		&op,
		(phys_addr_t) registered_domfb[vdev->otherend_id]->vaddr,
		GNTMAP_host_map | GNTMAP_readonly,
		registered_domfb[vdev->otherend_id]->gnt_handle);
	gnttab_unmap(&op);

	/* Free framebuffer area. */
	free_vm_area(registered_domfb[vdev->otherend_id]->area);

	/* Free vfb_domfb. */
	kfree(registered_domfb[vdev->otherend_id]);
	registered_domfb[vdev->otherend_id] = NULL;

	if (callback_rm_domfb) {
		callback_rm_domfb(vdev->otherend_id);
	}
}
\end{lstlisting}
