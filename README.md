# Support graphique dans SO3

Travail de Bachelor réalisé sous la direction du Prof. Daniel Rossier.

Première partie (SO3) :
* https://gitlab.com/smartobject/so3/-/issues/2
* https://discourse.heig-vd.ch/t/graphics-support-for-so3/41

Deuxième partie (SOO) :
* https://gitlab.com/smartobject/soo/-/issues/17
* https://discourse.heig-vd.ch/t/graphics-support-for-soo-refso3/129

Rendu final :
* [rapport-final.pdf](https://gitlab.com/nyg/rapport-final/-/blob/master/rapport-final.pdf)

Documentation Sphinx :
* [https://nyg.gitlab.io/so3-support-graphique/](https://nyg.gitlab.io/so3-support-graphique/)
