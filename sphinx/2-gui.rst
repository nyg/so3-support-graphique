Interface graphique
===================

Choix de LittlevGL
------------------

Le choix de la librairie graphique s’est porté sur LittlevGL, librairie
open-source écrite en C et sous licence MIT [#]_. Elle a été conçue
spécifiquement pour les systèmes embarqués et par conséquent ne requiert que peu
de ressources. La librairie permet aussi la copie des pixels de l’interface
graphique dans un framebuffer. Ce dernier étant une portion de la mémoire vive
dans laquelle s’écrit une représentation binaire d’un ensemble de pixels afin de
d’être affichés à l’écran.

.. figure:: images/lvgl-example.png
   :alt: Exemple d'interface graphique avec LittlevGL
   :align: center

   Exemple d’interface graphique tel que présenté sur le site web de LittlevGL

Initialement, cette librairie ne devait être utilisée que dans un premier temps
pour faciliter l’intégration d’une autre librairie plus complète (telles que Qt
ou GTK). Cependant, au vu des nombreuses fonctionnalités offertes par LittlevGL,
de sa documentation très complète et d’une communauté active, il a été décidé de
garder cette librairie jusqu’à la fin du travail de Bachelor.

Avec la sortie de la version 7 en mai 2020, le projet est renommé en LVGL
(*Light and Versatile Graphic Library*). Cependant, l’appellation LittlevGL est
gardée dans ce rapport car c’est la version 6.1.2 (février 2020) qui a été
intégrée à SO3.

Intégration et configuration
----------------------------

La librairie est composée d’un répertoire nommé ``lvgl`` contenant son code
source ainsi que plusieurs fichiers Makefile indiquant le chemin des sources
devant être compilées. Il n’existe cependant pas de Makefile compilant la
libraire.

Un fichier de configuration ``lv_conf.h`` est aussi fourni et doit être placé au
même niveau que le répertoire ``lvgl``. Il contient plusieurs paramètres tels
que la résolution maximale que doit supporter la librairie, le nombre de bits
par pixel, le niveau de log à afficher, etc.

La résolution maximale a été fixée à 1920x1080 ce qui est suffisant dans notre
contexte de système embarqué. Quant à la résolution effective — qui est limitée
par celle de l’écran utilisé — elle est communiquée à LittlevGL par
l’application utilisateur. Un autre paramètre modifié est le nombre de bits par
pixels, fixé à 32 — un octet pour chacune de ces valeurs : la transparence, le
rouge, le vert et le bleu.

La librairie a été placée dans l’espace utilisateur de SO3 et non pas dans le
noyau, car ce sont les applications utilisateur qui vont en faire usage. Elle a
été placée dans le répertoire ``so3/usr/lvgl``. L’arborescence est donc la
suivante :

.. figure:: images/lvgl-arb.png
   :alt: Arborescence de so3/usr/
   :align: center
   :scale: 60%

   Arborescence de ``so3/usr/``

Compilation
-----------

Il a fallu écrire le fichier ``lvgl/Makefile`` afin que celui-ci produise une
librairie statique pouvant être liée à l’exécutable de chacune des applications
écrites dans ``usr/src/``.

Le contenu de ce Makefile reste relativement simple, la cible ``all`` s’occupe
de compiler tous les fichiers C — lesquelles sont indiqués par les fichiers
Makefile (``*.mk``) susmentionnés — et de regrouper les fichiers objets
résultants dans l’archive ``liblvgl.a`` avec la commande ``ar``.

Il s’agissait ensuite de modifier le Makefile de ``usr/`` afin de lier la
librairie au fichier objet de chaque application. Pour cela, une nouvelle cible
créant la librairie a été écrite, puis celle-ci a été rajoutée comme dépendance
de la cible créant l’exécutable (fichier ELF) de chacune des applications.

.. code:: makefile

   out/%.elf: src/%.o $(LIBC_DIR)/$(NLIB) $(LVGL_DIR)/$(LVGL_LIB)
       $(LD) -o $@ libc/crt0.o libc/crt1.o $< \
             -L$(LIBC_DIR) -lso3              \
             -L$(LVGL_DIR) -llvgl $(LDFLAGS)

   $(LVGL_DIR)/$(LVGL_LIB):
       make -C lvgl all

Ainsi, une application utilisateur de SO3 peut désormais utiliser les fonctions
définies par LittlevGL simplement en incluant le fichier ``lvgl/lvgl.h``.

.. figure:: images/compil-so3-usr.svg
   :alt: Compilation d’une application SO3
   :align: center
   :scale: 200%

   Vue générale de la compilation d’une application SO3

Utilisation et fonctionnement
-----------------------------

Toute application voulant utiliser LittlevGL doit appeler certaines fonctions et
initialiser certaines structures de la librairie. Connaître ces différents
éléments permet de mieux définir et comprendre les modifications qui devront
être apportées à SO3. L’extrait suivant montre de manière simplifiée un exemple
d’application LittlevGL (un exemple complet et fonctionnel a été écrit dans
``usr/src/demo.c``).

.. code:: c

   #include "lvgl/lvgl.h"

   int main(int argc, char **argv)
   {
       /* Internal LittlevGL initialization. */
       lv_init();

       /* User-defined initializations. */
       fs_init();
       fbdev_init();
       inputdev_init();

       create_ui();

       /* LittlevGL must know how time passes by. */
       pthread_t thread;
       pthread_create(&thread, NULL, tick_routine, NULL);

       /* Run LittlevGL-defined tasks. */
       while (1) {
           lv_task_handler();
           usleep(5000);
       }

       return 0;
   }

   void *tick_routine(void *args)
   {
       /* Tell LittlevGL that 5ms passed. */
       while (1) {
           usleep(5000);
           lv_tick_inc(5);
       }
   }

LittlevGL initialise plusieurs de ses modules grâce à la fonction ``lv_init``,
son appel est donc obligatoire [#]_. Les fonctions d’initialisation ``*_init``
qui suivent sont définies par le développeur et doivent initialiser certaines
structures de LittlevGL. Ces structures seront vues plus en détails par la
suite.

Deux autres autres composants sont essentiels, le premier est un thread qui
indique à intervalle régulier le temps écoulé entre deux appels de la fonction
``lv_tick_inc`` [#]_ (fonction définie par LittlevGL). Cela permet à la
librairie de savoir quand exécuter certaines animations et tâches (e.g.
récupération des coordonnées de la souris). Le deuxième est l’appel toutes les 5
millisecondes (valeur recommandée par LittlevGL) de ``lv_task_handler``. Cette
fonction s’occupe d’exécuter des tâches définies soit par la librairie (e.g.
rafraîchissement de l’interface si changements) soit par le développeur (ce
n’est pas le cas ici) [#]_.

.. figure:: images/lvgl-app.svg
   :alt: Intéraction d'une application avec SO3
   :align: center

   Vue générale d’une application LittlevGL et de son interaction avec
   SO3

Affichage de l’interface
~~~~~~~~~~~~~~~~~~~~~~~~

Description et utilisation du framebuffer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Afin d’afficher l’interface graphique générée par LittlevGL à l’écran, il faut
pouvoir accéder et écrire dans le framebuffer depuis l’espace l’utilisateur de
SO3.

Le framebuffer et un espace mémoire [#]_ dans lequel est écrit un ensemble de
pixels. Il existe aussi un contrôleur de périphérique (circuit électronique)
dédié au framebuffer et permettant de le configurer (par exemple pour décider de
comment est représenté un pixel dans la mémoire — nombre de bits par couleur,
ordre des couleurs, etc.).

Par exemple, avec 24 bits par pixel (*bpp* — 8 pour chaque couleur rouge, vert
et bleu) il est possible de représenter plus de 16 millions de couleurs. Chaque
pixel est alors représenté sur quatre octets, le premier est soit ignoré soit
utilisé pour l’opacité, les suivants sont interprétés différemment selon la
configuration du contrôleur en RGB ou BGR (inversion du rouge et du bleu).

Afin de communiquer avec ce contrôleur, il est nécessaire d’écrire un driver qui
ne se situe pas dans l’espace utilisateur mais dans le noyau. Un driver est
écrit pour un contrôleur spécifique et chaque système embarqué possède son
propre ensemble de contrôleurs (à moins que des contrôleurs existants soit
réutilisés). C’est donc avec le driver du framebuffer qu’il faut communiquer
depuis l’espace utilisateur.

Dans le noyau Linux ainsi que dans les autres systèmes d’exploitation
s’inspirant d’Unix, il existe des fichiers spéciaux [#]_ (ou fichiers de
périphérique) qui n’existent pas sur le système de fichiers (e.g. FAT, NTFS)
mais lesquels donnent accès à un périphérique. Par exemple, un framebuffer est
représenté par un fichier nommé ``/dev/fbX``, ``X`` étant un identifiant
numérique. Cette technique est intéressante car elle permet aussi de gérer
d’autres périphériques tels que ceux d’entrée comme le clavier et la souris,
avec des noms de fichier comme ``/dev/keyboardX`` ou ``/dev/mouseX``.

Bien que SO3 puisse déjà ouvrir des fichiers normaux, les fichiers spéciaux ne
sont pas gérés et il a été décidé de les implémenter. L’implémentation est
décrite dans la section :ref:`special_files`.

Dans Linux, une fois le fichier du framebuffer ouvert et le descripteur de
fichier récupérer (grâce à l’appel système ``open``), une écriture dans ce
fichier correspondra à une écriture dans le framebuffer. Cependant, utiliser
l’appel système ``write`` de manière répétitive impactera les performances de
l’application.

Une technique plus performante consiste à rendre accessible l’espace mémoire
dédié au framebuffer dans l’espace utilisateur. Cela passe par l’appel système
``mmap`` et il n’est requis qu’une seule fois. Un pointeur sur l’espace mémoire
est alors récupéré et le framebuffer peut être écrit. Cet appel système n’existe
pas non plus dans SO3 et doit donc être implémenté.

Concrètement, voici le code nécessaire afin d’écrire un pixel dans le
framebuffer. Ce système est aussi ce qui permettra de copier l’interface
graphique de LittlevGL dans le framebuffer.

.. code:: c

   #include <fcntl.h>
   #include <sys/mman.h>

   /* Open the framebuffer. */
   int fd = open("/dev/fb0", O_WRONLY);

   /* Map the memory. */
   int *fbp = mmap(NULL, FB_SIZE, 0, 0, fd, 0);

   /* Write a red pixel in 24bpp RGB. */
   fbp[0] = 0x00ff0000;

Driver d’affichage LittlevGL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Dans la nomenclature de la librairie, un *driver* correspond a une structure C
permettant d’activer une fonctionnalité. À ces drivers sont attachées une ou
plusieurs fonctions *callback* qui sont appelées par la librairie au moment
propice.

Par exemple, au driver d’affichage [#]_ (*display driver*) il faut associer un
buffer que LittlevGL utilisera pour redessiner l’interface. Un callback doit
aussi être renseigné lequel copie ce buffer dans le framebuffer. Si ce buffer
est plus petit que l’écran, l’interface sera redessinée en plusieurs fois. Il y
a donc un compromis à faire entre utilisation de la mémoire et utilisation du
CPU. À noter que LittlevGL ne va redessiner que les parties de l’interface qui
ont changées.

Il est aussi possible de fournir à LittlevGL un deuxième buffer. Dans ce cas,
les opérations de rafraîchissement de l’interface et de copie dans le
framebuffer peuvent être faites en parallèle. Il faut cependant que l’OS
supporte l’accès direct à la mémoire (*Direct Memory Access*) ce qui n’est pas
le cas de SO3, ce deuxième buffer n’a donc pas été utilisé dans notre cas.

Le code source suivant montre comment doit être initialisé le driver ainsi que
le fonctionnement du callback qui utilise le pointeur sur le framebuffer
récupéré précédemment.

.. code:: c

   /* Buffer initialization. */
   static lv_color_t buf[LVGL_BUF_SIZE];
   static lv_disp_buf_t disp_buf;
   lv_disp_buf_init(&disp_buf, buf, NULL, LVGL_BUF_SIZE);

   /* Driver initialization. */
   static lv_disp_drv_t disp_drv;
   lv_disp_drv_init(&disp_drv);
   disp_drv.hor_res = scr_hres;
   disp_drv.ver_res = scr_vres;
   disp_drv.buffer = &disp_buf;
   disp_drv.flush_cb = my_fb_cb;
   lv_disp_drv_register(&disp_drv);

.. code:: c

   void my_fb_cb(lv_disp_drv_t *disp,
                 const lv_area_t *area,
                 lv_color_t *pixel)
   {
       lv_coord_t y, line_width = lv_area_get_width(area);
       uint32_t line_size = line_width * sizeof(lv_color_t);

       /* Copy line-by-line. */
       for (y = area->y1; y <= area->y2; y++) {
           memcpy(&fbp[y * scr_hres + area->x1], pixel, line_size);
           pixel += line_width;
       }

       lv_disp_flush_ready(disp);
   }

Gestion des périphériques d’entrée
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un accès au clavier et à la souris peut être obtenu de la même manière que cela
a été expliqué pour le framebuffer. Dans SO3, les drivers pour le clavier et la
souris n’existent pas, ils doivent donc être implémentés.

Une fois le descripteur de fichier obtenu, une opération de lecture ou
d’écriture n’aurait que peu de sens à moins de définir un protocole d’échange de
données (e.g. récupération des coordonnées de la souris). Les OS inspirés d’Unix
ont mis en place un appel système nommé ``ioctl`` [#]_ permettant d’exécuter des
opérations définies par un driver.

Cet appel système existe déjà dans SO3, seules les opérations nécessaires
doivent être implémentées. Pour le clavier, seule une opération pour récupérer
la dernière touche pressée est requise. Pour la souris, deux opérations doivent
être écrites. La première pour récupérer les coordonnées de la souris et la
deuxième afin d’indiquer au driver la résolution de l’écran. Cette information
est nécessaire afin que le driver puisse limiter les coordonnées de la souris,
afin d’empêcher celle-ci de « sortir » de l’écran.

Driver d’entrée – Souris
^^^^^^^^^^^^^^^^^^^^^^^^

L’initialisation du driver d’entrée LittlevGL [#]_ pour la souris et similaire à
celle du framebuffer. La fonction callback exécutera l’opération pour récupérer
les coordonnées de la souris et l’état de ses boutons. L’opération pour indiquer
la résolution de l’écran doit être exécutée après l’obtention du descripteur de
fichier de la souris.

.. code:: c

   int mfd = open("/dev/mouse0", 0);

   struct display_res res = { .h = scr_hres, .v = scr_vres };
   ioctl(mfd, IOCTL_MOUSE_SET_RES, &res);

   /* Driver initialization. */
   lv_indev_drv_t mouse_drv;
   lv_indev_drv_init(&mouse_drv);
   mouse_drv.type = LV_INDEV_TYPE_POINTER;
   mouse_drv.read_cb = my_mouse_cb;

.. code:: c

   bool my_mouse_cb(lv_indev_drv_t *indev, lv_indev_data_t *data)
   {
       static struct ps2_mouse mouse;
       ioctl(mfd, IOCTL_MOUSE_GET_STATE, &mouse);

       data->state = mouse->left ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
       data->point.x = mouse->x;
       data->point.y = mouse->y;

       return false;
   }

Driver d’entrée – Clavier
^^^^^^^^^^^^^^^^^^^^^^^^^

L’initialisation du driver LittlevGL pour le clavier suis exactement le même
principe. La fonction callback s’occupe par contre de récupérer la dernière
touche appuyée du clavier grâce à un l’appel ``ioctl``.

.. code:: c

   bool my_keyboard_cb(lv_indev_drv_t *indev, lv_indev_data_t *data)
   {
       static struct ps2_key key;
       ioctl(kfd, IOCTL_KB_GET_KEY, &key);

       if (key.value != 0) {
           data->key = key.value;
           data->state = key.state & 1;
       }

       return false;
   }

Accès au système de fichiers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LittlevGL fournit aussi un driver [#]_ pour accéder à des systèmes de fichiers
de l’OS. Cela permet par exemple d’afficher des images dans l’interface
graphique. Il faut initialiser un driver de ce type pour chaque système de
fichiers devant être accéder par LittlevGL. De manière similaire à ce qui est
fait sous Windows, une lettre est associée à chaque instance de driver
permettant ainsi d’identifier les fichiers (e.g. ``S:/mon-image.png``). SO3 ne
possédant qu’un seul système de fichiers, seul un driver est nécessaire.

Les extraits suivants montrent le code nécessaire afin d’afficher une image dans
l’interface graphique. Comme pour les drivers vus précédemment, plusieurs
fonctions callback sont utilisées. Ici, elles servent à faire le lien entre
LittlevGL et les fonctions C existantes de manipulation de fichier (e.g.
``fopen``, ``fread``, ``fclose``). Dans SO3, ces fonctions sont fournies par
musl [#]_, une librairie qui implémente la librairie C standard.

.. code:: c

   lv_fs_drv_t drv;
   lv_fs_drv_init(&drv);

   drv.letter = 'S';
   drv.file_size = sizeof(FILE*);
   drv.rddir_size = sizeof(FILE*);
   drv.ready_cb = fs_ready_cb;     /* Drive is ready to use.       */
   drv.open_cb = fs_open_cb;       /* Callback to open a file.     */
   drv.close_cb = fs_close_cb;     /* Callback to close a file.    */
   drv.read_cb = fs_read_cb;       /* Callback to read a file.     */
   drv.seek_cb = fs_seek_cb;       /* Callback to set file cursor. */
   drv.tell_cb = fs_tell_cb;       /* Callback to get file cursor. */
   lv_fs_drv_register(&drv);

.. code:: c

   lv_fs_res_t fs_open_cb(struct _lv_fs_drv_t *drv,
                          void *file_p,
                          const char *path,
                          lv_fs_mode_t mode)
   {
       FILE *fp = fopen(path, (mode & LV_FS_MODE_WR) ? "w" : "r");
       if (!fp) {
           return LV_FS_RES_UNKNOWN;
       }

       *((FILE **)file_p) = fp;
       return LV_FS_RES_OK;
   }

   lv_fs_res_t fs_read_cb(struct _lv_fs_drv_t *drv,
                          void *file_p,
                          void *buf,
                          uint32_t btr, /* bytes to read */
                          uint32_t *br) /* bytes read */
   {
       *br = fread(buf, sizeof(uint8_t), btr, *(FILE **)file_p);
       return LV_FS_RES_OK;
   }


.. _lvgl_app_detail:
.. figure:: images/lvgl-app-detail.svg
   :alt: Intéraction des drivers LittlevGL avec SO3
   :align: center
   :scale: 200%

   Vue générale des drivers LittlevGL et de leurs interactions avec SO3

La :numref:`lvgl_app_detail` illustre les interactions des drivers LittlevGL vu
précédemment avec le noyau SO3. Les drivers de SO3 ainsi que le *Virtual
Filesystem* seront abordés dans le chapitre suivant.

Application de démonstration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les extraits de code précédents font partie d’une application de démonstration
écrite afin de démontrer les fonctionnalités de LittlevGL mais aussi, et
surtout, de pouvoir s’assurer du bon fonctionnement des modifications qui seront
apportées à SO3. Cette application se trouve dans le fichier ``usr/src/demo.c``.

L’interface graphique de l’application (:numref:`demo_1` et :numref:`demo_2`),
composée de trois onglets, est basé sur du code de démonstration de LittlevGL
disponible sur leur dépôt Github [#]_. Le reste de l’application a été développé
spécifiquement pour SO3.

.. _demo_1:
.. figure:: images/demo-1.png
   :alt: Premier onglet de l’application de démonstration avec affichage
    d’une image
   :align: center

   Premier onglet de l’application de démonstration avec affichage d’une
   image

.. _demo_2:
.. figure:: images/demo-2.png
   :alt: Deuxième onglet de l’application de démonstration avec un champ
    texte
   :align: center

   Deuxième onglet de l’application de démonstration avec un champ texte

.. [#] https://lvgl.io
.. [#] https://docs.lvgl.io/latest/en/html/get-started/quick-overview.html#add-lvgl-into-your-project
.. [#] https://docs.lvgl.io/latest/en/html/porting/tick.html
.. [#] https://docs.lvgl.io/latest/en/html/porting/task-handler.html
.. [#] https://en.wikipedia.org/wiki/Framebuffer
.. [#] https://en.wikipedia.org/wiki/Device_file
.. [#] https://docs.lvgl.io/v7/en/html/porting/display.html
.. [#] https://en.wikipedia.org/wiki/Ioctl
.. [#] https://docs.lvgl.io/v7/en/html/porting/indev.html
.. [#] https://docs.lvgl.io/v7/en/html/overview/file-system.html
.. [#] https://musl.libc.org
.. [#] https://github.com/littlevgl/lv_examples/blob/master/lv_tests/lv_test_theme/lv_test_theme_1.c
