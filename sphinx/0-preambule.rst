Préambule
=========

La présente documentation Sphinx est une adaptation et amélioration a posteriori
du rendu final du Travail de Bachelor « Support graphique dans SO3 ». Travail
effectué lors du deuxième semestre de l’année scolaire 2019–2020, sous la
direction du Prof. Daniel Rossier.

Résumé du problème
------------------

La technologie *Smart Object Oriented* (SOO), mise au point par l’institut REDS
de l’HEIG–VD, permet la migration de systèmes d’exploitation (OS) en cours
d’exécution parmi un ensemble interconnectés de systèmes embarqués. Cette
technologie introduit la notion de *Smart Object*, qui fait référence à un
système embarqué tel qu’une Raspberry Pi 4, et celle de *Mobile Entity* (ME),
qui représente l’OS migrant en cours d’exécution.

La technologie SOO s’inscrit notamment dans le contexte de la domotique —
ensemble de techniques permettant le contrôle, grâce à l’informatique,
d’installations présentes dans un bâtiment. La possibilité pour une ME de
présenter une interface graphique à l’utilisateur — par exemple à travers un
écran tactile — ouvre la porte à de nombreux types d’applications. Une telle
interface permettrait à l’utilisateur de visualiser les données gérées par la ME
(e.g. données météorologiques) mais aussi d’interagir avec la ME (e.g.
modifications de paramètres, visualisation de données sous un autre format).

Ainsi, l’objectif de ce travail de Bachelor est l’ajout du support graphique et
de périphériques d’entrée dans SO3, OS développé par le REDS et utilisé comme
base des ME. Une librairie graphique existante devra être intégrée dans SO3,
d’abord lorsque celui-ci s’exécute de manière autonome. Puis, il s’agira de
créer une ME basée sur cette nouvelle version de SO3 et de l’intégrer dans le
framework SOO. Le développement se fera initialement dans un environnement
émulé, puis il devra être déployé sur une Raspberry Pi 4.

Cahier des charges
------------------

SO3
~~~

Le développement aura lieu sur la plateforme ARM Versatile Express qui sera
émulée par le logiciel QEMU.

Librairie graphique
^^^^^^^^^^^^^^^^^^^

Une librairie graphique existante doit être choisie et placée dans l’espace
utilisateur de SO3. La librairie nécessite d’être intégrée au système de
compilation de SO3 afin que les applications de cet OS puissent l’utiliser.

Une application de démonstration doit être développée afin de tester le bon
fonctionnement du développement effectué au cours de ce TB ainsi que de montrer
les possibilités offertes par la librairie (e.g. éléments graphiques).

Écriture de drivers
^^^^^^^^^^^^^^^^^^^

Framebuffer
'''''''''''

Il s’agit ici de trouver sur la plateforme émulée un contrôleur mettant à
disposition un framebuffer et d’écrire un driver permettant à SO3 de reconnaître
et d’utiliser ce contrôleur afin de pouvoir afficher une interface graphique à
l’écran.

Périphériques d’entrée
''''''''''''''''''''''

À nouveau, il faut d’abord trouver le où les contrôleurs permettant de
communiquer avec le clavier et la souris. Puis, les drivers doivent être écrits.
Ils permettront la récupération des événements générés par le clavier et la
souris.

Adaptation de SO3
^^^^^^^^^^^^^^^^^

Afin de pouvoir contrôler et interroger ces drivers, il est requis d’adapter et
de créer certains appels système (noyau SO3). Par exemple, il faut pouvoir
écrire dans l’espace mémoire dédié au framebuffer ou encore récupérer les
coordonnées de la souris.

Il est aussi demandé que cette communication avec les drivers puisse se faire
depuis une application SO3. Les appels systèmes modifiés doivent pouvoir être
appelés depuis l’espace utilisateur.

SOO
~~~

Cette seconde partie vise à créer une ME basée sur SO3 et pouvant être utilisée
par SOO. Cette ME doit intégrer les changements effectués lors de la première
partie. Le développement aura lieu sur la plateforme ARM Versatile Express
émulée par QEMU mais il s’agira aussi de déployer SOO et la ME sur une Raspberry
Pi 4. Cette partie se compose de plusieurs étapes.

Développement des interfaces virtuelles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les ME n’ont pas un accès direct au matériel. C’est SOO qui s’occupe de faire
l’intermédiaire entre la ME et le matériel par le biais d’interfaces virtuelles.
Ces interfaces nécessitent un développement autant du côté de la ME (SO3) que du
côté de SOO.

Il faut donc développer deux interfaces virtuelles, une pour le framebuffer et
une autre pour la gestion des entrées utilisateur (e.g. clavier, souris, écran
tactile).

Portage sur Raspberry Pi 4
^^^^^^^^^^^^^^^^^^^^^^^^^^

Les modifications apportées à la ME et à SOO doivent être portées sur la Pi 4
lorsque cela est nécessaire. Bien que la Pi 4 ait un processeur ARM, son
architecture est très différente de la plateforme Versatile Express et possède
d’autres contrôleurs.

Afin de tester l’intégration de la libraire graphique et le support des
périphériques d’entrée, plusieurs écrans tactiles seront choisis et proposés à
l’achat au REDS. Les écrans devront être testés avec la nouvelle ME. Le support
des événements tactiles devra aussi être ajouté.
