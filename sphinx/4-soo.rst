SOO
===

Introduction
------------

Dans la première partie de ce travail de Bachelor, le support graphique a été
ajouté à SO3 lorsque celui-ci est exécuté de manière autonome, c’est-à-dire
qu’il n’est pas virtualisé et donc qu’il possède un accès direct au matériel de
la plateforme sur laquelle il tourne.

Dans cette deuxième partie, il s’agit d’intégrer les changements apportés à SO3
dans une ME qui sera virtualisée par la technologie SOO. Le travail sera
effectué sur la ME nommée SOO.refso3 qui est basée, comme son nom l’indique, sur
SO3.

Pour comprendre les changements qui devront être effectués, il est important de
bien saisir les principaux composants de la technologie SOO et comment ils
interagissent. L’hyperviseur AVZ (*Agency Virtualizer*), développé par le REDS,
se lance en premier. Cet hyperviseur est de type 1 car c’est lui qui initie la
virtualisation des systèmes d’exploitation. Le premier OS virtualisé est
l’agence, un noyau Linux augmenté de plusieurs modifications apportées par le
REDS qui permettent, entre autres, la communication avec les différentes ME qui
sont virtualisées.

Autant l’agence que les différentes ME sont considérées comme des domaines.
L’agence est le domaine 0 et est le seul domaine à avoir accès au matériel de la
plateforme. Ainsi, dans une ME, il n’y a plus de notion de matériel. Pour ne pas
avoir à modifier l’espace utilisateur de la ME, il va falloir créer des drivers
virtuels qui auront pour but d’imiter le comportement des drivers de
périphériques comme, par exemple, celui du PL111 ou du PL050.

Intercommunication Agence–ME
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cependant, ces drivers virtuels auront quand même besoin de communiquer avec le
matériel, que se soit pour écrire dans le framebuffer ou pour récupérer les
événements des périphériques d’entrée. Pour cela, pour chacun des périphériques,
un driver backend doit être créé dans l’agence et un driver frontend doit être
créé dans la ME. Le driver backend, ou simplement backend, a accès au matériel
puisqu’étant dans l’agence. À noter qu’en général le backend ne communique pas
directement avec le matériel mais profite des drivers Linux existants. Le
frontend, présent dans la ME, communique avec le driver virtuel afin de simuler
l’existence du périphérique.

Évidemment, la communication entre le backend et le frontend est nécessaire.
Celle-ci est possible grâce à plusieurs mécanismes aussi implémentés par le
REDS. Il y a, par exemple, le VBStore qui permet d’écrire et de lire — depuis
l’agence ou la ME — une valeur à un chemin donné. Il existe aussi un anneau
partagé, qui permet l’échange de requêtes (initiées par le frontend) et de
réponses (initiées par le backend).

.. figure:: images/soo-intro.svg
   :alt: Interactions entre les drivers Linux, ceux du backend et
    frontend, et les drivers virtuels
   :align: center

   Interactions entre les drivers Linux, ceux du backend et frontend, et
   les drivers virtuels

Gestion de plusieurs ME
-----------------------

Il faut noter que plusieurs ME peuvent être virtualisées en même temps. Il y
aura donc plusieurs frontends pour le même périphérique. Par contre, il n’y aura
toujours qu’un seul backend par périphérique. Ainsi, dans le cas ou plusieurs ME
sont virtualisées, un backend communique avec plusieurs frontends.

Cela implique de savoir quel framebuffer de quel ME doit être affiché à l’écran
à un moment donné. Il faut aussi savoir vers quelle ME doivent être redirigées
les données des périphériques d’entrée. Lorsque l’agence a démarré et que
l’accès à la ligne de commande Linux est donné, il est possible de naviguer
parmi les domaines en tapant deux fois sur Ctrl + A.

Il a été décidé d’utiliser ce système afin de savoir quel domaine est actif à un
instant donné et quand est-ce qu’on passe d’un domaine à un autre. Cela permet
de décidé quel domaine doit voir son framebuffer affiché à l’écran et quel
domaine doit recevoir les données des périphériques d’entrées.

.. figure:: images/ctrl-a.png
   :alt: Exemple d’utilisation du double Ctrl + A
   :align: center

   Exemple d’utilisation du double Ctrl + A

C’est donc dans le fichier ``linux/soo/kernel/console/console.c`` que des
modifications ont été apportées. Initialement, il n’était possible de naviguer
qu’entre l’agence et la première ME. Une fonction plus générique nommée
``get_next`` a été créée qui retourne l’identifiant du prochain domaine. Pour
cela, il a été nécessaire d’utiliser la fonction ``get_ME_desc`` afin de détecté
si une ME est démarrée ou non.

Ensuite, dans la fonction ``avz_switch_console``, l’identifiant du domaine actif
est communiqué au backend du framebuffer et à celui des périphériques d’entrée
(qui seront détaillés plus loin). Ainsi, ces backends sont au courant du domaine
actif et peuvent donc agir en conséquence.

Gestion des framebuffers
------------------------

Création du backend et frontend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le backend a été crée dans ``linux/soo/drivers/vfbback/vfb.c`` et le frontend
dans ``SOO.refso3/so3/soo/drivers/vfbfront/vfb.c``. Le nom vfb correspond à
*virtual framebuffer* et respecte la convention actuelle des autres backends et
frontends (e.g. vuart, vdummy).

Afin que le backend et le frontend soient correctement compilés, un Makefile
contenant le nom du fichier objet devant être obtenu a été créé dans les
répertoires ``vfbback`` et ``vfbfront``. Le nom du fichier étant ``vfb.c``, par
convention le nom du fichier objet sera ``vfb.o``.

.. code:: makefile

   # vfbback/Makefile
   obj-$(CONFIG_VFB_BACKEND) := vfb.o

   # vfbfront/Makefile
   obj-$(CONFIG_VFB_FRONTEND) := vfb.o

La variable ``CONFIG_VFB_*`` fait partie du système de configuration Kconfig
utilisé par le noyau Linux et repris par SO3. Il permet d’activer ou de
désactiver des composants du noyau (e.g. drivers). Désactiver implique que le
composant n’est pas compilé et donc qu’un binaire plus petit est obtenu.

Il est alors possible de rendre la compilation du vfb optionnelle ce qui
permettrait de continuer à utiliser des ME sans interface graphique. Il faut
pour cela que les deux variables soient déclarées. Celle du backend doit l’être
dans ``soo/drivers/Kconfig`` et celle du frontend dans
``so3/soo/drivers/Kconfig``. L’extrait suivant illustre la déclaration de la
variable du backend.

.. code:: menuconfig

   config VFB_BACKEND
       bool "vfb framebuffer backend driver"
       default N
       help
         This driver enabled to deal with a virtualized interface for a framebuffer in the Mobile Entity.

La configuration du noyau (Linux ou SO3) se fait avec la commande ``make
menuconfig`` exécutée dans le répertoire contenant le Makefile principal. Cette
commande fournit une interface pour activer ou désactiver les composants du
noyau.

.. figure:: images/kconfig.png
   :alt: Activation du vfb dans le menu Kconfig de l’agence
   :align: center

   Activation du vfb dans le menu Kconfig de l’agence

Puisque le backend et frontend sont considérés comme des drivers par le noyau,
il est nécessaire que les fichiers DTS décrivant le matériel de la plateforme
référencent ces deux composants. L’extrait suivant montre les nœuds à ajouter.
Le backend est rajouter dans ``vexpress-tz.dts`` (Linux) et le frontend dans
``so3virt.dts`` (SO3).

.. code:: dts

   agency {
      backends {
         vfb {
            compatible = "vfb,backend";
            status = "ok";
         };
      }
   }

   ME {
      frontends {
         vfb {
            compatible = "vfb,frontend";
            status = "ok";
         };
      };
   };

Le backend et frontend sont maintenant compilés et seront initialisés lors du
démarrage de l’agence et de la ME. Le backend est toujours initialisé avant le
frontend car il fait partie des drivers de l’agence. Ce n’est qu’une fois
l’agence démarrée que la ME est à son tour lancée. Le backend et frontend vfb
n’ont pas été écrits à partir de zéro mais sont basés sur ceux du vdummy, qui
fournit une base fonctionnelle pour la création de drivers backend et frontend.
Une fois le fichier ``vdummy.c`` copié, il suffit de remplacer toutes les
occurrences de *vdummy* par *vfb*. Il ne faut pas oublier de faire de même pour
les headers (``vdummy.h``).

Emplacement du framebuffer
^^^^^^^^^^^^^^^^^^^^^^^^^^

Comme mentionné précédemment, il n’y a pas de notion de matériel dans une ME.
C’est-à-dire que, bien que la plateforme ARM Versatile Express continue d’être
utilisée, la ME n’a pas accès au contrôleur PL111, ni d’ailleurs à la VRAM mise
à disposition par la plateforme.

Il est alors nécessaire de réfléchir à l’endroit en mémoire où une ME doit
placer son framebuffer. Grâce à un mécanisme de SOO existant (les *grant tables*
ou tables d’octrois), il serait possible de donner accès à la VRAM à une ME afin
qu’elle y écrive l’interface graphique dessinée par l’application LittlevGL.

Cependant, cette solution pose deux problèmes. Premièrement, il faudrait mettre
en place un système qui n’autoriserait que la ME active à aller écrire dans ce
framebuffer. Sinon, chaque ME irait continuellement récrire ce que les autres ME
ont écrit dans le framebuffer.

Deuxièmement, ce système empêcherait à l’agence d’accéder au framebuffer de
chaque ME, indépendamment du domaine actif. En effet, parallèlement à ce travail
de Bachelor est mené celui de Tommy Girardi [#]_, lequel nécessite un accès
simultané à tous les framebuffers des ME virtualisées. Cela afin d’afficher sur
un même écran divisé en plusieurs parties le contenu de chaque framebuffer. De
ce fait, la solution proposée plus haut ne peut pas être considérée.

.. figure:: images/multi-fb.svg
   :alt: Emplacement de la mémoire allouée pour le framebuffer :
    solution choisie
   :align: center

   Emplacement de la mémoire allouée pour le framebuffer : solution
   choisie

La solution choisie consiste à allouer un espace mémoire pour chaque framebuffer
de ME. Cette allocation pourrait avoir lieu soit dans l’agence soit dans la ME.
Même si, techniquement, les deux cas sont possibles, il a été décidé d’effectuer
cette allocation dans la ME.

En effet, en allouant les espaces mémoire dans l’agence (c’est-à-dire par le
noyau Linux), il devient plus difficile de protéger ces framebuffers, notamment
d’écritures non désirées. Lorsque l’espace mémoire est alloué dans la ME, il est
possible de définir l’accès donné à l’agence comme étant en lecture seule.

Taille du framebuffer
^^^^^^^^^^^^^^^^^^^^^

La taille du framebuffer dépend de la résolution de l’écran et du nombre de bits
par pixel. Or, ces informations ne sont connues que par l’agence puisque c’est
elle qui a accès au matériel. Il faut donc communiquer ces informations à la ME
qui en a besoin afin d’allouer l’espace mémoire pour le framebuffer.

Obtention des propriétés du framebuffer
"""""""""""""""""""""""""""""""""""""""

Le noyau Linux possède un grand nombre de drivers pour différents types de
framebuffers (``drivers/video/fbdev``). Lorsque Linux détecte un contrôleur de
framebuffer, il l’initialise et ensuite enregistre une référence du framebuffer
dans le tableau ``registered_fb`` du fichier ``fb.h``. Le tableau contient des
pointeurs sur la structure ``fb_info``. C’est une structure complexe qui est
utilisée par Linux pour décrire et manipuler un framebuffer. Elle contient,
entre autres, une structure de type ``fb_var_screeninfo`` qui contient notamment
la résolution horizontale et verticale du framebuffer ainsi que le nombre de
bits par pixel.

.. figure:: images/vfbinfo.svg
   :alt: Initialisation du backend vfb
   :align: center

   Initialisation du backend vfb : recherche de la structure ``fb_info``

Pour la plateforme Versatile Express, c’est le driver ``amba-clcd.c`` qui est
utilisé par Linux pour initialiser le PL111. Cependant, le driver ne connaît pas
les valeurs à utiliser pour initialiser le framebuffer. Ces valeurs proviennent
de la DTS de la plateforme. L’extrait suivant est un nœud du fichier
``vexpress-v2m-rs1.dtsi`` décrivant le contrôleur PL111.

.. code:: dts

   clcd@1f0000 {
       compatible = "arm,pl111", "arm,primecell";
       memory-region = <&vram>;

       panel {
           panel-timing {
               clock-frequency = <65000000>;
               hactive = <1024>;
               vactive = <768>;
               hsync-len = <136>;
               hfront-porch = <20>;
               hback-porch = <160>;
               vfront-porch = <3>;
               vback-porch = <29>;
               vsync-len = <6>;
           };
       };
   };

On peut y distinguer la résolution voulue, :math:`1024 \times 768`, ainsi qu’une
référence à la VRAM qui est décrite dans la DTS ``vepxress-tz.dts``. Le nombre
de bits par pixel est calculé par le driver à partir de la propriété
``max-memory-bandwidth`` qui n’est pas définie dans l’extrait précédent. Le
driver choisi alors la valeur de 32 bits par pixel, ce qui nous convient.

Il s’agit maintenant de choisir le bon framebuffer du tableau ``registered_fb``.
Pour l’instant il n’est prévu d’avoir qu’un seul écran rattaché au système
embarqué, choisir le premier framebuffer du tableau est une option suffisante.
Cela est fait lors de l’initialisation du backend (``vfb_init``). La première
structure ``fb_info`` non nulle de ``registered_fb`` est choisie pour décider du
framebuffer qui sera utilisé. Elle est stockée dans une variable propre au
backend nommée ``vfb_info``. Une résolution minimale est cependant imposée afin
d’exclure les framebuffers qui auraient une résolution trop petite. Celle-ci a
été fixée arbitrairement à :math:`640 \times 480`.

Le backend étant considéré comme un driver Linux au même titre que ceux des
framebuffers, il n’est pas possible de garantir que le driver du framebuffer
sera initialisé avant le backend, autrement dit que le tableau ``registered_fb``
ait été rempli avant sa lecture par le backend. Cependant, Linux met un place un
système de notifications afin d’être notifier d’événements relatifs aux
framebuffers. Il faut utiliser la fonction ``fb_register_client`` définie dans
``fb_notify.c`` et lui passer une structure de type ``notifier_block``. Celle-ci
contient une fonction callback qui sera appelée à chaque événement relatifs aux
framebuffers.

Linux définit plusieurs événements de ce type mais ceux voulus ne sont
disponibles que pour la plateforme AM200 EPD. Il faut donc modifier le fichier
``fb.h`` afin que les événements ``FB_EVENT_FB_REGISTERED`` et
``FB_EVENT_FB_UNREGISTERED`` soient disponibles pour toutes les plateformes. Les
fonctions ``do_register_framebuffer`` et ``do_unregister_framebuffer`` de
``fbmem.c`` doivent aussi être modifiées pour que les événements soient bien
envoyés. Les modifications consistent simplement à enlever les ``#ifdef``
utilisé par Linux pour restreindre le code à la plateforme AM200 EPD.

À noter qu’il n’est pas nécessaire de recevoir les événements relatif aux
framebuffers si un framebuffer est détecté lors de l’initialisation du backend.

.. code:: c

   int vfb_init(void)
   {
       /* We check if there's already a registered framebuffer device. */
       int i = 0;
       while (i < FB_MAX && vfb_set_agencyfb(registered_fb[i]))
           i++;

       /* Otherwise, we register a client that will be notified when a
        * framebuffer is registered. */
       if (i == FB_MAX)
           fb_register_client(&vfb_fb_notif);

       return 0;
   }

Transmission des propriétés à la ME
"""""""""""""""""""""""""""""""""""

Pour transmettre la résolution du framebuffer aux ME, il a été décidé d’utiliser
VBStore et donc de créer un chemin dans lequel est créé une propriété pour la
résolution horizontale et une autre pour la résolution verticale. VBStore se
prête bien à cette utilisation car la résolution consiste en deux valeurs à
transmettre uniquement à l’initialisation de la ME. Il n’y a pas besoin de
mettre en place un anneau partagé, système plus complexe, juste pour transmettre
les propriétés du framebuffer.

Chaque backend possède son propre espace VBStore et doit être initialisé dans la
fonction ``vbstorage_agency_init`` du fichier ``vbstorage.c``. L’extrait suivant
montre comment il est créé.

.. code:: c

    np = of_find_compatible_node(NULL, NULL, "vfb,backend");
    if (of_device_is_available(np)) {
        vbs_store_mkdir("/backend/vfb");
    }

Dans cet extrait, la présence du nœud contenant une propriété avec la valeur
``vfb,backend`` est cherché dans la DTS et s’il est trouvé, le chemin
``/backend/vfb`` est créé. Un chemin est créé de la même manière pour chaque ME
et chaque frontend mais cela se passe dans le fichier ``vbstore_me.c``. Une
entrée doit être rajoutée pour le vfb comme le montre cet extrait de la fonction
``vbstore_devices_populate``.

.. code:: c

   fdt_node = fdt_find_compatible_node("vfb,frontend");
   if (fdt_device_is_available(fdt_node)) {
       DBG("%s: init vfb...\n", __func__);
       vbstore_dev_init(ME_domID(), "vfb", false, "vfb,frontend");
   }

Cela crée un chemin ayant le format suivant ``/devices/<domain-id>/vfb/0``.
Puisque la résolution du framebuffer est commune à chaque ME, on pourrait
l’écrire dans le VBStore du backend. Cependant, il a été choisi de l’écrire dans
le VBStore de la ME car cela faciliterait un éventuel développement futur qui
requerrait des résolutions différentes pour chaque ME.

Le chemin ``devices/<domain-id>/vfb/0/resolution`` est créé dans le backend avec
la fonction ``vbus_mkdir`` définie dans ``vbus_vbstore.c``. Puis, les deux
propriétés ``hor`` et ``ver`` sont créées avec la fonction ``vbus_printf``. Le
code suivante montre comment cela a été fait.

.. code:: c

   struct vbus_transaction vbt;
   char dir[40];

   vbus_transaction_start(&vbt);

   sprintf(dir, "device/%01d/vfb/0", vdev->otherend_id);
   vbus_mkdir(vbt, dir, "resolution");

   sprintf(dir, "device/%01d/vfb/0/resolution", vdev->otherend_id);
   vbus_printf(vbt, dir, "hor", "%u", FB_HRES);
   vbus_printf(vbt, dir, "ver", "%u", FB_VRES);

   vbus_transaction_end(vbt);

Dans la fonction ``vfb_probe`` du frontend, la résolution est récupérée grâce à
la fonction ``vbus_scanf`` définie dans ``vbus_vbstore.c``.

.. code:: c

   struct vbus_transaction vbt;
   uint32_t hres, vres;
   char dir[40];

   vbus_transaction_start(&vbt);
   sprintf(dir, "device/%01d/vfb/0/resolution", ME_domID());
   vbus_scanf(vbt, dir, "hor", "%u", &hres);
   vbus_scanf(vbt, dir, "ver", "%u", &vres);
   vbus_transaction_end(vbt);

Réservation de l’espace mémoire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Maintenant que la résolution est connue par le frontend, il est possible
d’allouer l’espace mémoire pour le framebuffer puisque sa taille peut être
calculée en multipliant la résolution horizontale, la résolution verticale et le
nombre d’octet par pixel.

Il n’y a pas toujours une correspondance exacte entre le nombre de bits par
pixel et le nombre d’octets par pixel. Par exemple, le PL111 définit le mode
24bpp mais un pixel sera quand même stocké sur 4 octets (un octet est ignoré).
Dans Linux ainsi que LittlevGL, on parle plutôt de 32bpp (4 octets aussi). Le
nombre de couleurs est le même (16 millions) mais au niveau logiciel le premier
octet peut être utilisé pour la transparence.

Actuellement, le nombre d’octets par pixel a été fixé à quatre dans le backend.
Si cela était amené à changer, il faudrait passer cette valeur au frontend via
le VBStore comme pour la résolution.

Une fois la taille calculée, il est possible d’allouer l’espace mémoire pour le
framebuffer. Cela est fait avec la fonction ``get_contig_free_pages`` qui
réserve un certain nombre de pages physiques. Ces pages n’appartiennent à aucun
processus et ne seront pas libérées, cela est très proche du fonctionnement de
la VRAM.

.. code:: c

   uint32_t fb_base, hres, vres, page_count;
   char dir[40];

   page_count = hres * vres * 4 / PAGE_SIZE;
   fb_base = get_contig_free_pages(page_count);
   BUG_ON(!fb_base);
   so3virt_fb_set_info(fb_base, hres, vres);

Création du driver virtuel
""""""""""""""""""""""""""

Le driver virtuel vient remplacer dans SOO.refso3 le driver du PL111 décrit dans
le chapitre `6 <#ch:so3>`__. Il a été écrit dans le fichier ``so3virt_fb.c``.
Comme pour les autres drivers, un nœud a du être rajouté dans la DTS
``so3virt.dts``.

.. code:: dts

   so3virt-fb {
       compatible = "fb,so3virt";
       status = "ok";
   };

Il définit aussi les deux opérations sur fichier ``mmap`` et ``ioctl`` et dans
sa fonction d’initialisation il enregistre une structure ``classdev`` afin que
le fichier spécial ``/dev/fb0`` soit disponible dans l’espace utilisateur de la
ME.

Les différences se trouvent dans la fonction d’initialisation, il n’y a pas de
périphérique à initialiser et donc pas de registres à écrire. Dans la fonction
``fb_mmap``, l’adresse physique de la VRAM y est remplacée par celle réservée
par le frontend. Le frontend communique cette adresse grâce à la fonction
``so3virt_fb_set_info`` qui informe aussi le driver virtuel de la résolution de
l’écran (nécessaire pour les opérations ``ioctl``).

Mapping du framebuffer dans l’agence
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour que l’espace mémoire réservé ci-dessus puisse être accédé par le backend,
le frontend doit autoriser cet accès pour toutes les pages qui composent
l’espace mémoire. Cela est fait juste après la réservation de l’espace mémoire
par la fonction ``gnttab_grant_foreign_access`` qui prend comme paramètres
l’identifiant du domaine de l’agence et la page à autoriser.

Cette fonction est définie dans ``gnttab.c`` (*grant table* ou table d’octrois)
et fait partie d’un mécanise existant qui permet justement le partage de pages
entre les différents domaines. La fonction retourne une référence vers l’entrée
nouvellement créée dans la table d’octrois. C’est cette valeur qui est
nécessaire au backend pour pouvoir accéder au framebuffer de la ME. La valeur
étant un entier elle peut être écrite dans le chemin
``device/%01d/vfb/0/domfb-ref`` de VBStore.

.. code:: c

   for (i = 0; i < page_count; i++) {
       res = gnttab_grant_foreign_access(vdev->otherend_id,
                                         phys_to_pfn(fb_base + i * PAGE_SIZE),
                                         1);
       BUG_ON(res < 0);

       if (i == 0) /* The back-end only needs the first grantref. */
           fb_ref = res;
   }

   sprintf(dir, "device/%01d/vfb/0/domfb-ref", ME_domID());
   vbus_printf(vbt, dir, "value", "%u", fb_ref);

Ce chemin a été préalablement créé par le backend dans la fonction
``vbf_probe``. Le chemin est aussi observé par le backend afin qu’il soit
notifié de toute modification de propriétés. Lorsqu’il y a une modification, la
fonction ``callback_me_domfb`` est appelée.

.. code:: c

   watches[vdev->otherend_id] = kzalloc(sizeof(struct vbus_watch), GFP_ATOMIC);
   vbus_watch_pathfmt(vdev,
                      watches[vdev->otherend_id],
                      callback_me_domfb,
                      "device/%01d/vfb/0/domfb-ref/value",
                      vdev->otherend_id);

C’est cette fonction qui alloue un espace mémoire dans l’agence et le mappe vers
l’espace réservé par le frontend (partagé via la table d’octrois). Le code
source suivant illustre ce procédé.

.. code:: c

   grant_ref_t fb_ref;
   struct gnttab_map_grant_ref op;
   struct vm_struct *area;
   domid_t domid; /* ME domain id */

   area = alloc_vm_area(FB_SIZE, NULL);

   gnttab_set_map_op(&op,
                     (phys_addr_t) area->addr,
                     GNTMAP_host_map | GNTMAP_readonly,
                     fb_ref, domid, 0, FB_SIZE);

   gnttab_map(&op);

Affichage des différents framebuffers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Comme expliqué dans la section `7.2 <#sec:gst-multi-me>`__, il est possible de
changer le domaine actif en tapant Ctrl + A deux fois. Avec les modifications
apportées au fichier ``console.c``, cette combinaison de touches appelle la
fonction ``vfb_set_active_domfb`` du backend.

Le but de cette fonction est de modifier les registres du PL111, notamment le
registre LCDUPBASE afin d’y mettre l’adresse physique du framebuffer à afficher.
Il est possible de faire cela en modifiant certaines valeurs de la structure
``fb_info`` du framebuffer puis d’appeler l’opération ``fb_set_par`` qui va
écrire les nouvelles valeurs de la structure dans les registres du contrôleur.
L’appel de cette opération (définie dans la structure ``fb_info``) exécutera la
fonction ``clcdfb_set_par`` puis ``clcdfb_set_start``, laquelle modifie les
registres voulus.

La valeur de ``fb_info`` qui doit être modifiée est l’adresse physique de
l’emplacement du framebuffer, soit ``fb_info->fix.smem_start``. Par souci de
cohérence, l’adresse virtuelle sera aussi modifiée, elle se trouve dans
``fb_info->screen_base``.

Afin d’enregistrer les adresses physiques et virtuelles des différents
framebuffers, la structure ``vfb_domfb`` a été créée, elle représente un
framebuffer de domaine. En effet, une telle structure ne sera pas seulement
utilisée pour les ME mais aussi pour l’agence. Elle contient différentes valeurs
:

-  ``domid_t id`` : l’identifiant du domaine auquel appartient le framebuffer,
-  ``uint64_t paddr`` : l’adresse physique communiquée par le frontend,
-  ``char *vaddr`` : l’adresse virtuelle du framebuffer,
-  ``struct vm_struct *area`` : un pointeur vers l’espace mémoire alloué par le
   backend,
-  ``grant_handle_t gnt_handle`` : l’identifiant du mapping effectué par le
   backend.

Les deux dernières valeurs sont utiles lorsqu’une ME est terminée, cela permet
de dé-mapper l’espace alloué puis de le libérer. C’est la fonction ``vfb_close``
du backend qui s’occupe de libérer ces ressources.

Il y a donc une structure ``vfb_domfb`` pour chaque domaine devant afficher un
framebuffer. Ces structures sont créées dans la fonction ``callback_me_domfb``
vue précédemment (à l’exception de la structure pour l’agence) et sont stockées
dans le tableau ``registered_domfb``. L’indice du tableau correspond à
l’identifiant du domaine. La taille du tableau correspond donc au nombre maximal
de domaines pouvant être virtualisés. Ce nombre est repris du fichier
``console.c`` et est fixé à 8 (à noter que l’hyperviseur est inclus dans ce
nombre même si ce n’est pas un domaine et qu’il ne possède pas de framebuffer).

.. code:: c

   struct gnttab_map_grant_ref op;
   struct vm_struct *area;
   struct vfb_domfb *fb;
   domid_t domid;

   fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
   fb->id = domid;
   fb->paddr = op.dev_bus_addr << 12;
   fb->vaddr = area->addr;
   fb->area = area;
   fb->gnt_handle = op.handle;
   vfb_set_domfb(fb); /* added to registered_domfb */

L’agence possède aussi sa structure ``vfb_domfb`` car il est intéressant de
pouvoir revenir sur le framebuffer de l’agence. Il est en effet possible que
celui-ci ait été modifié par une autre application. La structure est créée dans
la fonction ``vfb_set_agencyfb`` qui est appelée lorsque la structure
``fb_info`` a été détectée, c’est-à-dire lors l’initialisation du backend ou
lorsqu’un framebuffer est enregistré par Linux et que le backend en est notifié.

.. code:: c

   static int vfb_set_agencyfb(struct fb_info *info)
   {
       struct vfb_domfb *fb;

       if (vfb_info
           || !info
           || info->var.xres < MIN_FB_HRES
           || info->var.yres < MIN_FB_VRES) {
           return -1;
       }

       vfb_info = info;

       fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
       fb->id = 0;
       fb->paddr = info->fix.smem_start;
       fb->vaddr = info->screen_base;

       vfb_set_domfb(fb);
       vfb_set_active_domfb(fb->id);

       return 0;
   }

Il est nécessaire d’appeler ``vfb_set_active_domfb`` car bien que le PL111 soit
détecté par Linux et que la structure ``fb_info`` soit initialisée, les
registres du contrôleur n’ont pas été modifiés. L’appel est donc nécessaire pour
que ``clcdfb_set_par`` soit appelé.

En activant le driver Linux *Framebuffer Console*, le contrôleur sera activé et
l’appel à ``vfb_set_active_domfb`` ne sera pas nécessaire. Cependant,
l’activation de ce driver pose problème car il crée un thread pour le
clignotement du curseur de la console. Or, lorsque la structure ``fb_info`` est
modifiée (lors d’un changement du domaine actif) le thread ne peut plus écrire
dans l’espace mémoire et fait donc planter le noyau Linux. Ce driver n’étant pas
requis, il a été décidé de le considéré comme toujours désactivé.

Notifications extérieures
^^^^^^^^^^^^^^^^^^^^^^^^^

Une partie du travail de Bachelor de Tommy Girardi requiert d’être informée
lorsque le framebuffer d’une ME est créé puis détruit (suite à la terminaison de
la ME). Puisque l’adresse virtuelle du framebuffer est nécessaire, il a été
décidé d’implémenter un simple système de callback.

La fonction ``vfb_set_callback_new_domfb`` permet de définir la fonction à
appeler lorsqu’une nouvelle entrée est rajoutée au tableau ``registered_domfb``.
La fonction doit avoir la signature suivante : ``void (*cb)(struct vfb_domfb *,
struct fb_info *)`` et sera appelée à la fin de la fonction
``callback_me_domfb``.

La fonction ``vfb_set_callback_rm_domfb`` définit la fonction devant être
appelée lorsqu’une ME est terminée, son prototype est ``void (*cb)(domid_t id)``
et elle sera appelée dans la fonction ``vfb_close`` du backend.

Terminaison d’une ME
^^^^^^^^^^^^^^^^^^^^

La terminaison d’une ME implique que plusieurs ressources doivent être libérées.

-  La structure ``vbus_watch`` utilisée pour être notifié des
   modifications du chemin contenant la ``grant_ref`` doit être libérée
   après avoir été desenregistrée.
-  L’espace mémoire alloué par le backend doit être dé-mappé puis libéré
   (structure ``vm_struct``).
-  L’entrée ``vfb_domfb`` doit aussi être supprimée du tableau et
   libérée.

.. code:: c

   void vfb_close(struct vbus_device *vdev)
   {
       struct gnttab_unmap_grant_ref op;

       /* Unregister and free the watch. */
       unregister_vbus_watch(watches[vdev->otherend_id]);
       kfree(watches[vdev->otherend_id]);
       watches[vdev->otherend_id] = NULL;

       /* Unmap the grantref. */
       gnttab_set_unmap_op(
           &op,
           (phys_addr_t) registered_domfb[vdev->otherend_id]->vaddr,
           GNTMAP_host_map | GNTMAP_readonly,
           registered_domfb[vdev->otherend_id]->gnt_handle);
       gnttab_unmap(&op);

       /* Free framebuffer area. */
       free_vm_area(registered_domfb[vdev->otherend_id]->area);

       /* Free vfb_domfb. */
       kfree(registered_domfb[vdev->otherend_id]);
       registered_domfb[vdev->otherend_id] = NULL;

       if (callback_rm_domfb) {
           callback_rm_domfb(vdev->otherend_id);
       }
   }

Gestion des périphériques d’entrée
----------------------------------

Événements d’entrée
^^^^^^^^^^^^^^^^^^^

Le module *input* est un module du noyau Linux par lequel passe tous les
événements d’entrée, qu’ils proviennent d’une souris, d’un clavier, d’un écran
tactile ou encore d’autres périphériques d’entrée. L’existence de ce module a
grandement simplifié l’écriture du backend et frontend des périphériques
d’entrée, nommé vinput.

En effet, Linux possédant déjà de multiples drivers pour les périphériques
d’entrée, il n’est pas nécessaire de se préoccuper des particularités de chacun,
comme cela a été fait avec le PL050. De plus, pour chaque événement reçu, le
driver du périphérique appelle la fonction ``input_event`` définie dans le
module input de Linux (``input.c``). Un tel événement [#]_ est composé de trois
valeurs (déclarées dans ``include/uapi/linux/input-event-codes.h``) :

-  ``unsigned int type`` : désigne le type d’événement, il en existe une
   douzaine mais ceux qui sont intéressants pour ce travail sont
   ``EV_KEY`` (e.g. touches de clavier, boutons de souris), ``EV_REL``
   (déplacement de la souris) et ``EV_ABS`` (déplacement de l’écran
   tactile).

-  ``unsigned int code`` : un code identifiant plus précisément
   l’événement, par exemple le code 42 pour le type ``EV_KEY`` désigne
   la touche du clavier *Left Shift*, le code 0 pour le type ``EV_REL``
   désigne le déplacement de la souris selon l’axe des abscisses.

-  ``int value`` : la valeur de l’événement, pour une touche clavier,
   indique si elle a été appuyée ou relâchée, pour un déplacement de la
   souris c’est la valeur de ce déplacement qui est indiqué.

Afin de récupérer ces événements dans le backend vinput, il suffit de modifier
la fonction ``input_event`` afin d’appeler la fonction ``vinput_pass_event``
définie dans le backend qui s’occupera de transmettre l’événement au frontend.

.. figure:: images/vinput-gen.svg
   :alt: Transmission des événements d’entrée des drivers Linux vers le
    backend vinput
   :align: center

   Transmission des événements d’entrée des drivers Linux vers le
   backend vinput

Création du backend et frontend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le processus de création du backend et du frontend est très similaire à celui du
framebuffer. Il est basé sur vdummy et a été placé dans le répertoire
``vinput``. Il faut créer un nouveau nœud dans la DTS Linux et celle de la ME,
ainsi que modifier les fichiers Kconfig correspondants afin que les drivers
puissent être activés ou désactivés dans la configuration du noyau.

La différence principale entre le vfb et vinput se trouve dans la communication
entre le backend et le frontend. Dans vfb, trois propriétés dans VBStore ont
suffit. Par contre, utiliser VBStore pour l’envoi d’événements n’est pas adapté
car il est possible qu’ils soient produits (par l’agence) plus vite qu’ils ne
soient consommés (par la ME).

Il est plus adapté d’utiliser un anneau partagé entre le backend et le frontend.
Cet anneau permet au backend de déposer des réponses sur l’anneau, le frontend
peut ensuite lire les réponses à sa guise. Le backend notifie le frontend via
une interruption, ce qui est très similaire au fonctionnement du PL050 et des
périphériques d’entrée en général. De la même manière, le frontend peut déposer
des requêtes sur l’anneau et notifier le backend. À noter qu’une réponse ne
présuppose pas une requête, les deux notions sont indépendantes.

Dans le cas du vinput, seules les réponses sont nécessaires car les événements
d’entrées ne sont transmis que du backend vers le frontend. Une réponse
correspond à une structure. Afin de transmettre les événements d’entrée, la
structure ``vinput_response_t`` a été déclarée telle que :

.. code:: c

   typedef struct  {
       unsigned int type;
       unsigned int code;
       int value;
   } vinput_response_t;

Gestion des ME
^^^^^^^^^^^^^^

Lorsqu’un frontend est connecté, la fonction ``vinput_connected`` du backend est
exécutée. Cette fonction a été modifiée pour garder une référence des frontend
connectés. Un tableau a été créé à cet effet et contient un pointeur sur une
structure ``vinput_t``. L’indice du tableau correspond à l’identifiant du
domaine.

Lorsque le domaine actif change suite à un double Ctrl + A,
``vinput_set_current`` est appelée. Cette fonction enregistre simplement
l’identifiant du domaine actif passé en paramètre dans la variable
``current_vinput``.

Ainsi, afin de savoir à quel frontend transmettre les événements d’entrée, il
suffit d’accéder à l’élément ``current_vinput`` du tableau ``vinputs``. Il est
important de restreindre les événements d’entrée à la seule ME active, car ces
événements proviennent de l’utilisateur qui voit à l’écran l’interface graphique
de la ME active.

Transmission des événements
^^^^^^^^^^^^^^^^^^^^^^^^^^^

La figure `7.2 <#fig:vinput-ring>`__ montre l’utilisation de l’anneau partagé
afin de transmettre les événements d’entrée du backend au frontend et ensuite
vers les drivers virtuels.

.. figure:: images/vinput-ring.svg
   :alt: Transmission des événements d’entrée du backend au frontend via
    l’anneau partagé
   :align: center

   Transmission des événements d’entrée du backend au frontend via
   l’anneau partagé

Du backend au frontend
""""""""""""""""""""""

La fonction ``vinput_pass_event``, appelée par le module input de Linux, fait
usage de l’anneau partagé afin de transmettre les événements au frontend.

.. code:: c

   void vinput_pass_event(unsigned int type, unsigned int code, int value)
   {
       vinput_response_t *ring_rsp;
       vinput_t *vinput = vinputs[current_vinput];

       /* If the frontend is not connected, skip. */
       if (!vinput)
           return;

       /* Send event to front-end. */
       ring_rsp = vinput_ring_response(&vinput->ring);
       ring_rsp->type = type;
       ring_rsp->code = code;
       ring_rsp->value = value;

       vinput_ring_response_ready(&vinput->ring);
       notify_remote_via_virq(vinput->irq);
   }

La fonction ``notify_remote_via_irq`` permet, comme son nom l’indique, de
notifier le frontend de l’existence d’une nouvelle réponse via une interruption.
Cela aura pour effet d’exécuter la fonction ``vinput_interrupt`` du frontend,
puisqu’elle a été définie comme étant le gestionnaire d’interruptions. L’extrait
de code suivant montre comment récupérer la réponse du backend.

.. code:: c

   irq_return_t vinput_interrupt(int irq, void *dev_id)
   {
       struct vbus_device *vdev = (struct vbus_device *) dev_id;
       vinput_t *vinput = to_vinput(vdev);
       vinput_response_t *ring_rsp;
       unsigned int type, code;
       int value;

       while ((ring_rsp = vinput_ring_response(&vinput->ring)) != NULL) {

           type = ring_rsp->type;
           code = ring_rsp->code;
           value = ring_rsp->value;

           /* Handle input event. */
       }

       return IRQ_COMPLETED;
   }

Du frontend aux drivers virtuels
""""""""""""""""""""""""""""""""

Deux drivers virtuels ont été créés dans la ME : ``so3virt_kbd.c`` pour la
clavier et ``so3virt_mse.c`` pour la souris. Leur but est la création d’un
fichier ``/dev/keyboard0`` et ``/dev/mouse0`` depuis l’espace utilisateur mais
aussi de recevoir et traiter les événements reçus du frontend. Puisque ces
drivers n’interagissent pas avec un contrôleur de périphérique comme le PL050,
le protocole PS2 n’est plus utilisé, simplifiant grandement ces drivers.

Afin qu’aucune modification ne soit nécessaire dans l’espace utilisateur, les
drivers doivent réagir aux mêmes opérations ``ioctl`` définies dans le driver du
PL050. C’est-à-dire retourner la dernière touche appuyée pour le driver du
clavier et retourner les coordonnées de la souris pour le driver de la souris.

Driver clavier
''''''''''''''

Dans le driver du clavier, la fonction ``so3virt_kbd_event`` a été créée et
permet au frontend de passer les événements clavier au driver virtuel. Dans
cette fonction, le driver doit transformer les événements d’entrée en un
caractère UTF-8. Il faut détecter si la touche Shift est appuyé lorsqu’un
événement correspondant à une lettre est reçu. Si c’est le cas alors il faut
retourner la lettre majuscule correspondante.

À noter que si une touche est relâchée, sa valeur n’est pas mise à zéro dans la
fonction ``so3virt_kbd_event``, mais seulement une fois qu’elle a été lue par un
appel ``ioctl``. Cela afin d’être sûr que le client ait le temps de lire la
touche. Un système plus robuste aurait pu être implémenté en utilisant une file
où les événements seraient lus par le driver virtuel lors d’un appel ``ioctl``.

S’il avait été nécessaire de gérer des touches (ou combinaison de touches) qui
ne retournent pas un caractère mais qui exécutent une action (e.g. raccourci
clavier), il aurait été plus judicieux de gérer les événements dans l’espace
utilisateur, ces actions étant propres à chaque application.

Driver souris
'''''''''''''

Dans le driver de la souris, la fonction ``so3virt_mse_event`` a été créée et
permet au frontend de passer les événements relatif à la souris au driver
virtuel. Dans cette fonction, les coordonnées absolues de la souris doivent être
calculées et les boutons de la souris doivent être enregistrés.

Les événements d’entrée de type ``EV_REL`` n’envoie pas les coordonnées absolues
de la souris mais le déplacement de celle-ci par rapport à l’événement précédent
et selon un axe (abscisse ou ordonnée). Il faut alors calculer les coordonnées
absolues de la souris tout comme cela est fait pour le driver PL050.

Séparation des événements clavier–souris
''''''''''''''''''''''''''''''''''''''''

Dans la fonction ``vinput_interrupt`` le frontend doit déterminer quels
événements sont envoyés au clavier et lesquels à la souris. Cela a été fait en
regardant le type de l’événement mais aussi le code.

.. code:: c

   while ((ring_rsp = vinput_ring_response(&vinput->ring)) != NULL) {

       type = ring_rsp->type;
       code = ring_rsp->code;
       value = ring_rsp->value;

       if (type == EV_REL || /* is mouse movement */
          (type == EV_KEY && /* is mouse button click */
            (code == BTN_LEFT || code == BTN_MIDDLE || code == BTN_RIGHT)) {
           so3virt_mse_event(type, code, value);
       }
       else if (type == EV_KEY) {
           so3virt_kbd_event(type, code, value);
       }

       /* Skip unhandled events. */
   }

Portage sur Raspberry Pi 4
--------------------------

Introduction
^^^^^^^^^^^^

Jusqu’à présent, autant SO3 que SOO ont été testés sur la plateforme ARM
Versatile Express émulée par QEMU. Cela a été fait pour faciliter et accélérer
le développement mais cette situation ne correspond pas à la réalité d’une mise
en production de SOO.

Le déploiement de SOO sur la Raspberry Pi 4 [#]_ est un objectif de ce travail
de Bachelor. Ce déploiement permet de tester le travail réalisé dans de vraies
conditions et en utilisant un périphérique d’entrée comme un écran tactile sur
lequel est aussi affiché l’interface graphique.

.. figure:: images/rpi-4-hardware.jpg
   :alt: Composants de la Raspberry Pi 4
   :align: center

   Composants de la Raspberry Pi 4 [#]_

Écrans tactiles
^^^^^^^^^^^^^^^

Trois écrans tactiles ont été achetés par le REDS afin de pouvoir les tester sur
la Pi 4 et aussi de comparer les différences entre ces écrans, notamment la
taille et résolution mais aussi la manière dont ils se connectent à la Pi 4
(e.g. HDMI, DSI). Après recherche et comparaison de multiples écrans, ceux
choisis sont :

#. l’écran officiel de la `fondation Raspberry
   Pi <https://www.raspberrypi.org/products/raspberry-pi-touch-display/>`__
   d’une taille de 7" et d’une résolution de :math:`800 \times 480`,

#. un écran de la compagnie
   `52Pi <https://www.52pi.com/7-inch-lcds/45-52pi-7-inch-1024x600-capacitive-touch-screen-hdmi-tft-lcd-display-for-raspberry-pibeagle-bone-blackwindows-10macbook-pro.html>`__
   d’une taille de 7" aussi mais d’une résolution plus élevée de
   :math:`1024 \times 600`,

#. un écran de la compagnie
   `Waveshare <https://www.waveshare.com/10.1inch-HDMI-LCD-B-with-case.htm>`__
   d’une taille de 10.1" et d’une résolution de :math:`1280 \times 800`.

Le premier écran se connecte à la Pi 4 sur son port DSI. L’alimentation de
l’écran peut être faite grâce au port micro-USB ou via les ports GPIO. Les
événements d’entrée sont transférés via le port DSI.

Le deux derniers écrans se connectent sur le port micro-HDMI de la Pi 4. Leur
alimentation ne peut être faite que par un port micro-USB présent sur chacun des
écrans.

Gestion des événements tactiles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Fonctionnement avec Raspbian
""""""""""""""""""""""""""""

Avec l’OS Raspbian — distribution officiel de la fondation Raspberry Pi basée
sur Debian — les trois écrans sont reconnus automatiquement. Aucune
configuration de l’OS n’est nécessaire.

Fonctionnement avec SOO
"""""""""""""""""""""""

Les écrans n’ont pas fonctionné du premier coup lorsque la Pi 4 démarrait avec
SOO. Le premier problème est qu’ils ne s’allumaient pas, bien que le branchement
fut le même. La raison était que les ports USB n’étaient pas détectés par SOO et
que, par conséquent, les écrans n’étaient pas alimentés.

Plusieurs solutions ont été testées mais sans succès. D’abord, différents
drivers ont été activés dans le noyau Linux, notamment ceux relatifs aux
contrôleurs USB, xHCI et PCIe. Puis, les fichiers DTS de SOO pour la Pi 4 ont
été comparés avec ceux de la dernière version du noyau Linux, lequel offre un
meilleur support de la Pi 4. Les fichiers DTS les plus récents n’ont pas pu être
importés tels quels car ceux de SOO comportent plusieurs modifications relatives
à SOO. Les fichiers ont alors été comparés un à un et les modifications qui
auraient pu avoir un lien avec les ports USB ont été intégrée à SOO, notamment
un bloc relatif au bus PCIe, ce dernier étant utilisé par les ports USB.

Après une recherche plus approfondie du Prof. Daniel Rossier, il semblerait que
les adresses utilisées par le bus PCIe soient sur 64 bits alors que l’OS est
compilé en 32 bits. Il faudrait donc que SOO soit compilé avec l’option LPAE
(*Large Physical Address Extensions*) pour que les ports USB fonctionnent.

Le non support des ports USB par SOO a amené un deuxième problème concernant les
événements d’entrée des écrans tactile. En effet, alors que les câbles USB
alimentant les écrans peuvent être branchés — à des fins de test — sur un
ordinateur à proximité, les événements devant être transmis via USB ne peuvent
alors pas être reçu par SOO. Une souris ou un clavier ne peuvent pas non plus
être branchés.

Par conséquent, sur la Pi 4, les différents périphériques d’entrée n’ont pas été
testés lors de ce travail de Bachelor.

Implémentation du support des événements tactiles
"""""""""""""""""""""""""""""""""""""""""""""""""

Malgré l’impossibilité de tester les événements tactiles, il a quand été décider
d’ajouter un support basique de ceux-ci. Ces événements sont aussi transmis par
les différents drivers à la fonction ``input_event`` mentionnée précédemment et
se composent donc aussi des trois valeurs ``type``, ``code`` et ``value``.

Événements absolus
''''''''''''''''''

Le support de deux types d’événements envoyés par l’écran a été décidé. Le
premier concerne les événements de type ``EV_ABS`` permettant de déplacer le
curseur à l’écran. Contrairement à la souris, les événements possèdent une
abscisse et une ordonnées. Par contre, ces coordonnées ne sont pas relatives à
la résolution de l’écran. Lorsque l’écran tactile et connecté à la Pi 4, il
communique au driver une valeur minimale et maximale pour chacun des deux axes
et les coordonnées y sont relatives.

Cette valeur peut être récupérée dans la fonction ``input_register_device`` du
fichier ``input.c``. Depuis cette fonction est appelée une nouvelle fonction
``vinput_set_touch_bounds`` créée dans le backend. Les valeurs minimales et
maximales sont récupérées et enregistrées dans deux tableaux du frontend comme
illustré dans les extraits de codes suivants.

.. code:: c

   int input_register_device(struct input_dev *dev)
   {
       /* ... */

       if (test_bit(EV_ABS, dev->evbit) && dev->absinfo) {
           vinput_set_touch_bounds(dev->absinfo);
       }

       /* ... */
   }

.. code:: c

   static int32_t min[] = {0, 0};
   static int32_t max[] = {1000, 1000}; /* Test values. */

   void vinput_set_touch_bounds(struct input_absinfo *info)
   {
       min[0] = info[0].minimum; /* x */
       max[0] = info[0].maximum;
       min[1] = info[1].minimum; /* y */
       max[1] = info[1].maximum;
   }

Ces valeurs ne sont pas transmises au frontend mais sont utilisées directement
par le backend pour modifier les événements ``EV_ABS`` afin de ramener les
coordonnées à la résolution de l’écran. Cela est fait dans la fonction
``vinput_pass_event`` du backend. Ainsi, le frontend reçoit directement les
coordonnées de l’événement et peut les transmettre telles quelles au driver
virtuel qui modifiera ensuite la position du curseur.

Événements « touch »
''''''''''''''''''''

L’écran émet aussi un événement lorsque l’utilisateur tape du doigt sur l’écran.
Dans ce cas c’est un événement de type ``EV_KEY`` avec le code ``BTN_TOUCH`` qui
est envoyé. Actuellement, un tel événement sera envoyé au driver du clavier par
le frontend. Il faut cependant qu’il soit envoyé à celui de la souris afin de
simuler un clique avec le bouton gauche de la souris. La condition envoyant les
événements au driver de la souris et modifiée comme suit :

.. code:: c

   if (type == EV_REL || /* is mouse movement */
      (type == EV_KEY && /* is mouse button click */
       (code == BTN_LEFT || code == BTN_MIDDLE || code == BTN_RIGHT || code == BTN_TOUCH)) ||
      (type == EV_ABS && /* is touchscreen event */
       (code == ABS_X || code == ABS_Y))) {

       so3virt_mse_event(type, code, value);
   }

Gestion des framebuffers
^^^^^^^^^^^^^^^^^^^^^^^^

Détection de la résolution
""""""""""""""""""""""""""

Avec Raspbian, la résolution des écrans est détectée automatiquement. Avec SOO,
il est nécessaire de communiquer cette résolution dans le fichier
``config.txt``. Ce fichier agit comme le BIOS sur un ordinateur du bureau [#]_.
Communiquer la résolution implique qu’il faille modifier le fichier lorsque l’on
change d’écran.

L’extrait suivant montre la configuration nécessaire pour les deux derniers
écrans. Pour l’écran officiel de Raspberry, aucune modification n’est
nécessaire. En effet, les modifications suivantes ne concernent que les écrans
HDMI.

.. code:: c

   # 52Pi - 7"
   hdmi_group=2
   hdmi_mode=87
   hdmi_cvt=1024 600 60

   # Waveshare - 10.1"
   hdmi_group=2
   hdmi_mode=27

Un mode correspond à une résolution et une fréquence d’affichage. Si la
résolution voulue n’existe pas, il faut créer un nouveau mode et indiquer la
résolution ainsi que la fréquence manuellement avec la propriété ``hdmi_cvt``.
C’est qui est fait pour l’écran 52Pi.

Création du framebuffer
"""""""""""""""""""""""

Le framebuffer de la Pi 4 est géré par un processeur multimédia nommé
*VideoCore* et développé par Broadcom. La documentation sur ce processeur est
extrêmement limitée, surtout pour la version 6 qui est très différente de la
version 4 embarquée sur la Raspberry Pi 3 [#]_.

Lorsque la résolution est correctement mise dans le ``config.txt``, le
framebuffer est correctement créé par Linux et est présent dans le tableau
``registered_fb``. Cependant, il a été remarqué que la fonction ``fb_set_par``
de la structure ``fb_info`` n’était pas définie et donc qu’il était impossible
de modifier l’adresse du framebuffer devant être affiché à l’écran.

Afin d’avoir plus d’information sur le driver utilisé pour le framebuffer du
VideoCore, la fonction ``dump_stack`` a été utilisée dans la fonction
``register_framebuffer`` de ``fbmem.c``. Elle permet d’afficher la trace
d’appels ayant mené à cette fonction. Il a été découvert que le driver utilisé
est un driver générique nommé *Simple Framebuffer*. Il semblerait alors que
l’adresse mémoire physique du framebuffer est déterminée par le VideoCore et ne
puisse pas être changée.

Cela pose un problème car le système implémenté jusqu’à présent repose sur la
précondition que l’adresse du framebuffer puisse être changée.

Affichage des différents framebuffers – Adaptation
""""""""""""""""""""""""""""""""""""""""""""""""""

La solution implémentée afin de palier cette différence fut d’implémenter un
thread copiant périodiquement le framebuffer du domaine actif dans le
framebuffer du VideoCore, mais seulement quand le domaine actif est une ME.

Par contre, cela aura pour effet d’effacer le framebuffer de l’agence. Il faut
donc allouer un espace mémoire dédié au framebuffer de l’agence. Cet espace
mémoire ne sera utilisé que pour sauvegarder et restaurer l’état du VideoCore
lorsque l’agence est le domaine actif. Cette allocation est faite dans la
fonction ``vfb_set_agencyfb``. L’extrait suivant montre la création de la
structure ``vfb_domfb`` pour l’agence ainsi que l’allocation mémoire.

.. code:: c

   static int vfb_set_agencyfb(struct fb_info *info)
   {
       struct vfb_domfb *fb;
       fb = kzalloc(sizeof(struct vfb_domfb), GFP_ATOMIC);
       fb->id = 0;

   #ifdef CONFIG_ARCH_VEXPRESS
       /* ... */
   #endif

   #ifdef CONFIG_ARCH_BCM2835 /* Pi 4 */
       fb->vaddr = vmalloc(FB_SIZE);

       vfb_set_domfb(fb);
       vfb_set_active_domfb(fb->id);

       /* Start the framebuffer copy thread. */
       BUG_ON(!kthread_run(kthread_domfb_cpy, NULL, "fb-copy-thread"));
   #endif

       return 0;
   }

La sauvegarde de l’état du VideoCore dans le framebuffer de l’agence ne doit
être fait seulement lorsque l’on passe de l’agence à une ME (via le double Ctrl
+ A). Cette sauvegarde est donc faite dans la fonction ``vfb_set_active_domfb``
grâce au code suivant :

.. code:: c

   void vfb_set_active_domfb(domid_t domid)
   {
       /* ... */

       if (active_domfb == 0 && domid != 0) {
           memcpy(registered_domfb[0]->vaddr, vfb_info->screen_base, FB_SIZE);
       }
   }

Puis, la restauration du framebuffer de l’agence dans celui du VideoCore ce fait
dans le thread de copie, mais seulement lorsque l’on passe d’une ME à l’agence.

.. code:: c

   static int kthread_domfb_cpy(void *arg)
   {
       void *addr_domfb, *addr_vc = vfb_info->screen_base;
       uint32_t previous_domfb = 0;

       while (true) {
           if (active_domfb != 0 || previous_domfb != 0) {
               previous_domfb = active_domfb;
               addr_domfb = registered_domfb[previous_domfb]->vaddr;
               memcpy(addr_vc, addr_domfb, FB_SIZE);
           }

           msleep(50);
       }

       return 0;
   }

Même si cette solution n’est utilisée pour le moment que sur la Pi 4, son
avantage est qu’elle pourrait aussi l’être pour un autre contrôleur qui ne
permet pas le changement de l’adresse du framebuffer. Il est aussi tout à fait
possible d’utiliser cette solution pour tous les contrôleurs de framebuffer même
si dans certains cas les performances seront dégradées à cause du thread de
copie. Cependant, cela permettrait de ne pas avoir à gérer plusieurs
architecture comme c’est maintenant le cas.

.. [#] « Smart Object Oriented – SOO.display Système d’affichage collaboratif » Tommy Girardi
.. [#] https://www.kernel.org/doc/html/latest/input/event-codes.html
.. [#] https://www.raspberrypi.org
.. [#] https://www.waveshare.com/img/devkit/Raspberry-Pi-4-Model-B/Raspberry-Pi-4-Model-B-intro.jpg
.. [#] https://www.raspberrypi.org/documentation/configuration/config-txt/README.md
.. [#] https://en.wikipedia.org/wiki/VideoCore#Data_sources
