Support graphique dans SO3
==========================

.. toctree::
   :caption: Table des matières
   :maxdepth: 3
   :numbered:

   0-preambule.rst
   1-intro.rst
   2-gui.rst
   3-so3.rst
   4-soo.rst
   5-conclusion.rst
