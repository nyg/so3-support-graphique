Introduction
============

L’institut REDS [#]_ de l’HEIG–VD a mis au point la technologie *Smart Object
Oriented* (SOO) [#]_ [#]_ permettant la migration d’environnements en cours
d’exécution parmi un ensemble interconnectés de systèmes embarqués. Dans ce
contexte, la notion de *Smart Object* représente un système embarqué tel qu’une
Raspberry Pi 4 et celle de *Mobile Entity* (ME) se réfère à l’environnement
migrant en cours d’exécution.

SOO se présente sous la forme d’un framework conçu autour du noyau Linux et d’un
hyperviseur basé sur les concepts XEN de virtualisation pour systèmes embarqués.
Une Mobile Entity est un système d’exploitation (OS) virtualisé par cet
hyperviseur. C’est dans ce cadre que SO3 [#]_ (*Smart Object Oriented Operating
system*) a été développé à partir de zéro par le REDS, fournissant ainsi un OS
léger et facilement modifiable.

La technologie SOO s’inscrit notamment dans le contexte de la domotique —
ensemble de techniques permettant le contrôle, grâce à l’informatique,
d’installations présentes dans un bâtiment. La possibilité pour une ME de
présenter une interface graphique à l’utilisateur — par exemple à travers un
écran tactile — ouvre la porte à de nombreux types d’applications. Une telle
interface permettrait à l’utilisateur de visualiser les données gérées par la ME
(e.g. données météorologiques) mais aussi d’interagir avec la ME (e.g.
modifications de paramètres, visualisation de données sous un autre format).

L’objectif de ce travail de Bachelor est donc l’ajout du support graphique dans
SO3. Cela passe par le choix et l’intégration d’une librairie graphique
existante dans SO3 et non pas par l’implémentation depuis zéro d’une telle
librairie. Ce travail se déroule en deux étapes. Premièrement, il s’agit
d’implémenter le support graphique lorsque SO3 s’exécute de manière autonome
(i.e. en dehors du framework SOO). Il est aussi nécessaire de supporter des
périphériques d’entrée comme le clavier et la souris afin d’interagir avec
l’interface graphique.

Deuxièmement, il s’agit de faire fonctionner cette interface graphique dans une
ME basée sur SO3 avec les modifications susmentionnées. Lorsque la ME est
virtualisée, la communication avec le matériel ne se fait que dans SOO et non
pas dans la ME. Afin que la ME puisse tout de même interagir avec le matériel,
il faut implémenter des interfaces virtuelles pour l’affichage de l’interface
graphique et le support des périphériques d’entrée. Le développement se fera
d’abord dans un environnement émulé (QEMU), puis il s’agira de l’adapter pour
être déployé sur une Raspberry Pi 4.

.. [#] Reconfigurable & embedded Digital Systems
.. [#] http://blog.reds.ch/?p=1020
.. [#] https://gitlab.com/smartobject/soo
.. [#] https://gitlab.com/smartobject/so3
